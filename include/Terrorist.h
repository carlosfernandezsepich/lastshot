/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Terrorist.h
 * Author: marcado
 *
 * Created on 7 de junio de 2017, 18:59
 */

#ifndef TERRORIST_H
#define TERRORIST_H

#include "Player.h"
#include "TrackManager.h"
#include "SoundFXManager.h"
#include "GameManager.h"

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"		
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBulletCollisionsRay.h"

using namespace Ogre;
using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;

enum StateType {vigilando, atentando, huyendo, muerto};

class Terrorist {
public: //, Ogre::Entity *pEntityAK
    Terrorist(int pId, Ogre::Entity *pEntityModel, OgreBulletDynamics::RigidBody *pRigidBody, OgreBulletDynamics::DynamicsWorld *pWorld, Player *pPlayer);
    Terrorist(const Terrorist& orig);
    virtual ~Terrorist();
    
    void loadAnimations();
    void updateAnimations(Ogre::Real pDeltaT);
    void run(bool pValor, Ogre::Vector3 pDirection);
    void moveW(bool pValor, Ogre::Vector3 pDirection);
    void moveS(bool pValor, Ogre::Vector3 pDirection);
    void moveA(bool pValor, Ogre::Vector3 pDirection);
    void moveD(bool pValor, Ogre::Vector3 pDirection);
    void stop();
    void rotate(std::string pType);
    void crouch(bool pValor);
    
    RigidBody* getRigidBody(){return _rigidBody;}
    void setDeltaT(Ogre::Real deltaT) {_deltaT = deltaT;}
    Ogre::Real auxX();
    Ogre::Real auxY();
    void setKeyPressed(OIS::KeyCode pKey) {key = pKey;}
    Ogre::int32 getLife(){return test;}
    int getId(){return _id;}
    void setLife(int pLife){life = pLife;}
    Vector3 createRay();
    void exitGame(){_exitGame = true;}
    bool getExitGame() {return _exitGame;}
    Ogre::Real decideAxeY();
    Ogre::Real decideAxeX();
    void changeStateToHuyendoAnims(); //Prepara todo para que las animaciones de huir se ejecuten perfectamente
    void stopAllAnimations();
    void changeStateAtackToVigilandoNormal();
    void changeStateAtackToVigilandoProf();
    void changeStateVigilandoToAtackProf();
    Ogre::Real getTimeNoWatchPlayer(){return _timeNoWatchPlayer;}
    bool checkDead();
    void desactivePhysical();
    
    
    //ACCIONES
    void shootProfessional();
    void shootNormal();
    void shoot();   //Disparar
    void reload();  //Recargar
    void aim();     //Apuntar
    void dead();    //Morir
    void deadCrouch(); //Morir agachado
    

    
    //Funciones miembro
    void Inicializar(void);
    void ChangeState(StateType State);
    void ChangeState(char State);
    void UpdateState(void);
    StateType Informar(void);
    void reduce(Ogre::int32 i);
    void parche();
    
    
    
    
private:
    int _id;
    int _bullets;
    bool playerDetected;
    Ogre::int32 test;
    Ogre::Real _timeNoWatchPlayer;
    bool watchPlayer;
    bool _exitGame;
//    bool activatedHerido;
//    bool overlayPlayer;
    int life;
//    int typeAtack;
    std::string directionRotation;
//    Ogre::Real _timeNoDetectedPlayer;
    Ogre::Real _timeShoot;
//    Ogre::Real _timeEvitarColli;
    Ogre::Real _deltaT;
    OgreBulletDynamics::DynamicsWorld *world;
//    Ogre::Entity *_entityAK;
    Ogre::Entity *_entityModel;
    OgreBulletDynamics::RigidBody *_rigidBody;
    //---------------RUN-------------------
    Ogre::AnimationState *_AnimEnemyRunModel;
    //---------------WALK------------------
    Ogre::AnimationState *_AnimEnemyWalkModel;
    //-------------CROUCH-------------------
    Ogre::AnimationState *_AnimEnemyCrouchModel;
    //-------------SHOOT-------------------
    Ogre::AnimationState *_AnimEnemyShootModel;
    //-------------SHOOT-CROUCH------------
    Ogre::AnimationState *_AnimEnemyShoCroModel;    
    //-------------RELOAD-------------------
    Ogre::AnimationState *_AnimEnemyReloadModel;
    //-------------IDLE-------------------
    Ogre::AnimationState *_AnimEnemyIdleModel;
    //--------------AIM-------------------
    Ogre::AnimationState *_AnimEnemyAimModel;    
    //----------SHOOT-CROUCH--------------
    Ogre::AnimationState *_AnimEnemyShootCrouchModel;    
    //-------------AIM-CROUCH--------------     
    Ogre::AnimationState *_AnimEnemyAimCrouchModel;    
    //------------SHOOT-CROUCH-------------     
    Ogre::AnimationState *_AnimEnemyReloadCrouchModel;    
    //----------------DEAD-----------------     
    Ogre::AnimationState *_AnimEnemyDeadModel;    
    //------------DEAD-CROUCH--------------     
    Ogre::AnimationState *_AnimEnemyDeadCrouchModel;    
    
    OIS::KeyCode key;
    Player *player;
    StateType CurrentState; //Estado actual
    bool playerBlood; 
    
};

#endif /* TERRORIST_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Configuration.h
 * Author: test
 *
 * Created on 29 de marzo de 2017, 17:46
 */

#ifndef CONFIGURATION_H
#define CONFIGURATION_H
#include <Ogre.h>

class Configuration {
public:
    Configuration();
    Configuration(const Configuration& orig);
    virtual ~Configuration();
     Ogre::Real getSpeedMoveMouse(){return speedMoveMouse;} 
private:
    Ogre::Real speedMoveMouse = 20;
};

#endif /* CONFIGURATION_H */


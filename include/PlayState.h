/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include "Configuration.h"
#include "GameState.h"
#include "TrackManager.h"
#include "SoundFXManager.h"
#include "Weapon.h"
#include "Score.h"
#include "ScoreManager.h"

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"		
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBulletCollisionsRay.h"


#include <OgreOverlaySystem.h>
#include <OgreOverlayElement.h>
#include <OgreOverlayManager.h>
#include <OgreFontManager.h>
#include <OgreTextAreaOverlayElement.h>

#include "Terrorist.h"
#include "IA.h"
#include "Player.h"

using namespace Ogre;
using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;

#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED 
#include <boost/filesystem.hpp>
namespace fs = ::boost::filesystem;


class PlayState : public Ogre::Singleton<PlayState>, public GameState
{
 public:
  PlayState () {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
  
  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static PlayState& getSingleton ();
  static PlayState* getSingletonPtr ();


 protected:
  Weapon *myWeapon;
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera; Ogre::Vector2 mRot;
  CEGUI::Window* _sheet;
  IA *aIntelligence;
  Player *p;
  OIS::KeyCode KeyPressed;
  OIS::MouseButtonID MousePressed;

 
  
  //Nodes and Entities for WEAPON
  Ogre::Entity *_entM4; Ogre::SceneNode *_nodeM4;
  Ogre::Entity *_entM4Cock; Ogre::SceneNode *_nodeM4Cock;  
  Ogre::Entity *_entM4Magazine; Ogre::SceneNode *_nodeM4Magazine;  
  Ogre::Entity *_entM9; Ogre::SceneNode *_nodeM9;
  Ogre::Entity *_entM9_2; Ogre::SceneNode *_nodeM9_2;
  Ogre::Entity *_entM9Cargador; Ogre::SceneNode *_nodeM9Cargador;
  Ogre::Entity *_entBody; Ogre::SceneNode *_nodeBody;
  Ogre::Entity *_entM9Gatillo; Ogre::SceneNode *_nodeM9Gatillo;   
  //-----------------------------
  
  //-----------Blood-------------
  Ogre::SceneNode *_nodeBlood;
  Ogre::Entity * _entBlood;
  Ogre::AnimationState *_AnimBlood;
  //-----------------------------
  Ogre::Entity *_entExp;
  Ogre::Entity *_entBullet;  
  //----------Controller------------
  Ogre::SceneNode *_nodePerson;
  Ogre::SceneNode *_nodeParentPerson;
  Ogre::SceneNode *_nodeParentWeapon;
  //--------------------------------
  Ogre::SceneNode *_nodeExp;
  Ogre::SceneNode *_nodeBullet;
  
  //-----------TEST-------------
  
  
  //------Lights------
  Ogre::Light *_light;
  Ogre::Light *_light2;
  Ogre::Light *_light3;
  //------------------
  
  //Static Geometry and Entities for Map and Trees
  Ogre::StaticGeometry *_map;       Ogre::Entity *_entMap;
  Ogre::StaticGeometry *_tree01;    Ogre::Entity *_entTree01;
  Ogre::StaticGeometry *_leaves01;  Ogre::Entity *_entLeaves01;
  Ogre::StaticGeometry *_tree02;    Ogre::Entity *_entTree02;
  Ogre::StaticGeometry *_leaves02;  Ogre::Entity *_entLeaves02;
  Ogre::StaticGeometry *_tree03;    Ogre::Entity *_entTree03;
  Ogre::StaticGeometry *_leaves03;  Ogre::Entity *_entLeaves03;
  Ogre::StaticGeometry *_tree04;    Ogre::Entity *_entTree04;
  Ogre::StaticGeometry *_leaves04;  Ogre::Entity *_entLeaves04;
  Ogre::SceneNode *_mapNode;
  //-----------------------------------------------
 
  bool _gameOver;
  bool _panelCreated;
  bool _panelCreatedWin;
  Ogre::Real _gameOverTime;
  Ogre::Real _gameWinTime;
  int _contador; //Temporal
  bool _exitGame;
  bool _shoot;
  bool _reload; 
  bool _presenIsFinished;
  bool _positionWeapon;
  float _timeLastShoot;
  int num;
  Ogre::Real _deltaT;           //Time last frame
  Ogre::Real _timeForStar; // TIme for star game..
  Ogre::Real _timeFinished;
  Ogre::Real _timeSound;
  
  float _timeGame; //
  bool _gameStarted;
  bool _gameFinished;
  bool _soundReproduced;
  int score;
  int myArm;                    //MyArm

  Ogre::AnimationState *_Deploy;
  Ogre::AnimationState *_A1;
  Ogre::AnimationState *_A2;
  Ogre::AnimationState *_B1;
  Ogre::AnimationState *_B2;
  Ogre::AnimationState *_C1;
  Ogre::AnimationState *_C2;
  Ogre::AnimationState *_D1;
  Ogre::AnimationState *_D2;
  
  OgreBulletDynamics::DynamicsWorld * _world;
  OgreBulletCollisions::DebugDrawer * _debugDrawer;
  int _numEntities;
  float _timeLastObject;

  std::deque <OgreBulletDynamics::RigidBody *>         _bodies;
  std::deque <OgreBulletCollisions::CollisionShape *>  _shapes;

  OgreBulletDynamics::RigidBody *_rigidBox;
  
  Ogre::OverlayManager* _overlayManager;
  OverlayContainer* _panelGoGame;
  Ogre::Overlay *_overlayGoGame;
  Ogre::ParticleSystem *_particleBlood;
  Ogre::SceneNode *_nodeParticleBlood; 
  bool bloobEnemy;
  Ogre::Real timeBloodEnemy;
  
  
  private:
//------NEW FUNCTIONS------     
  void loadResources();
  std::vector<string> muestra_contenido_de(const std::string &a_carpeta);
  void loadMap(std::vector<string> pMeshes);

  void updateAnimations();
  void createTerrorist(std::vector<string> pMeshes); Terrorist *unTerrorista; std::vector<Terrorist*> terrorists;

  void initializeIA();

  
  void loadMachine();
  void setLights();
  void createCamera();
  
  int decideScore(Ogre::Real y);
  void createInfoTimePnl();
  void createInfoBulletsPnl();
  void createInfoLifePnl();
  void createOverlay();
  void createReticle();
  void createInfoBullets();
  void updateInfoBullets(Ogre::String pText, Ogre::String pText2);
  void createInfoTime();
  void updateInfoTime(Ogre::String pText);
  void updateInfoScore(Ogre::String pText);
  void createPanelGoGame(); 
  void updatePanelGoGame(int pNumber);
  void saveScore();
  void createPanelSaveScore();
  void createPanelGameOver();
  void createPanelWin();
  void deletePanelSaveScore();
  Score *_score;
  bool _panelSaving;
  int lifePlayer;
  void updateInfoLife(string life);
  //void createBlood();
  //void simulateBlood(Ogre::Vector3 position);
  void createBloodParticle(Vector3 position);
};

#endif



#ifndef ENEMY_H
#define ENEMY_H

#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreEntity.h>


class Enemy {
public:
    Enemy(int pId, Ogre::SceneNode *_nodeEnemy, Ogre::SceneNode *_nodeArm);
    Enemy(const Enemy& orig);
    virtual ~Enemy();
    Ogre::SceneNode* getSNenemy(){return _nodeEnemy;}
    Ogre::SceneNode* getSNarm(){return _nodeArm;}
    bool getState(){return _state;}
    int getId(){return _id;}
    void setState(bool pState){_state = pState;}
private:
    int _id;
    bool _state;
    Ogre::SceneNode *_nodeEnemy;
    Ogre::SceneNode *_nodeArm;
   
};

#endif /* ENEMY_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EnemyManager.h
 * Author: marcado
 *
 * Created on 26 de mayo de 2017, 12:54
 */

#ifndef ENEMYMANAGER_H
#define ENEMYMANAGER_H

#include <vector>
#include "Enemy.h"
#include <string> 
#include <iostream>

using namespace std;

class EnemyManager {
public:
    EnemyManager();
    EnemyManager(const EnemyManager& orig);
    virtual ~EnemyManager();
    std::vector<Enemy*> getEnemyList(); //Devuelve la lista de enemigos
    void addEnemyList(Enemy *pEnemy); //Agrega un enemigo a la lista
    void deleteEnemyList(Enemy *pEnemy); //Elimina un enemigo de la lista
    void changeState(Enemy *pEnemy); //Cambia el estado a muerto
private:
    std::vector<Enemy*> _enemies;

};

#endif /* ENEMYMANAGER_H */


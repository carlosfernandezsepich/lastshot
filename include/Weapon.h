

#ifndef WEAPON_H
#define WEAPON_H

class Weapon {
  int ammo_total; //Total de balas disponibles
  int capacity_charger; //Capacidad del cargador
  int ammo_charger; //Balas actualmente en el arma
  int id;

public:
  Weapon();
  Weapon(const Weapon& orig); 
  virtual ~Weapon(); // ES: destructor EN: destructor

  void shoot();
  bool is_empty_total();
  bool is_empty_charger();
  int get_ammo_total();
  int get_ammo_charger();
  void add_ammo_total(int amount);
  void reload_charger();
  void setCapacity(int cant);
  int getCapacity();
  int getIdBullet();

};

#endif /* WEAPON_H */


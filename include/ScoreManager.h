/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ScoreManager.h
 * Author: esi
 *
 * Created on 15 de febrero de 2017, 16:46
 */

#ifndef SCOREMANAGER_H
#define SCOREMANAGER_H

#include <iostream>
#include <string>
#include <Score.h>
#include <fstream>
#include <string>
#include <sys/stat.h>
#include <vector>
#include <sstream>
#include <algorithm>
#include <string>


using namespace std;

class ScoreManager {
public:
    ScoreManager();
    static std::vector<Score> getScores();
    static void write_file( string pName, int pScore );
    static std::vector<string> getListScores();
    static std::vector<string> orderList(std::vector<string> pList);
    static int getOnlyInt(string pRow);
private:
    static string file_name; //Nombre archivo
    static ifstream fs; //Leer archivo
    
    static bool openFile(const string name, const bool doCheck);
    static void closeFile();
};

#endif /* SCOREMANAGER_H */
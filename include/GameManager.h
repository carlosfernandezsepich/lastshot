/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#ifndef GameManager_H
#define GameManager_H

#include <stack>
#include <Ogre.h>
#include <OgreSingleton.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include <string>
#include "InputManager.h"
#include "Score.h"
#include "TrackManager.h"
#include "SoundFXManager.h"


class GameState;

class GameManager : public Ogre::FrameListener, public Ogre::Singleton<GameManager>, public OIS::KeyListener, public OIS::MouseListener
{
 public:
  GameManager ();
  ~GameManager (); // Limpieza de todos los estados.

  // Para el estado inicial.
  void start (GameState* state);
  
  // Funcionalidad para transiciones de estados.
  void changeState (GameState* state);
  void pushState (GameState* state);
  void popState ();
  
  // Heredados de Ogre::Singleton.
  static GameManager& getSingleton ();
  static GameManager* getSingletonPtr ();
  
   // Manejadores del sonido.
  TrackManager* _pTrackManager;
  SoundFXManager *_pSoundFXManager;
  TrackPtr _mainTrackMenu;
  TrackPtr _mainTrack;
  TrackPtr _menuStateTrack;
  //SoundFXPtr _simpleEffect;
  //SoundFXPtr _explosionEffect;
  TrackPtr _gameWin;
  TrackPtr _heridoFX;
  SoundFXPtr _akShootFX;
  SoundFXPtr _akReloadFX;
  SoundFXPtr _akEmptyFX;
  SoundFXPtr _m9ShootFX;
  SoundFXPtr _m9ReloadFX;
  SoundFXPtr _m9EmptyFX;
  SoundFXPtr _m4ShootFX;
  SoundFXPtr _m4ReloadFX;
  SoundFXPtr _m4EmptyFX;
  SoundFXPtr _voiceEnemyDownFX;
  SoundFXPtr _voiceLetsgoFX;
  SoundFXPtr _voiceMissionFailedFX;
  SoundFXPtr _voiceMissionCompletedFX;  
  SoundFXPtr _shootEffect;
  SoundFXPtr _rechargeEffect;
  SoundFXPtr _emptyEffect;
  SoundFXPtr _introStateEffect;
  SoundFXPtr _terroristAttack;
  std::string _namePlayer;
  
  TrackPtr getGameWinPtr () { return _gameWin; }
  TrackPtr getTrackMenuPtr () { return _mainTrackMenu; }
  TrackPtr getTrackPtr () { return _mainTrack; }
  TrackPtr getHeridoFX () { return _heridoFX; }   
  TrackPtr getMenuStateTrack () { return _menuStateTrack; }
  SoundFXPtr getAKshootFX () { return _akShootFX; } 
  SoundFXPtr getAKreloadFX () { return _akReloadFX; } 
  SoundFXPtr getAKemptyFX () { return _akShootFX; } 
  SoundFXPtr getM9shootFX () { return _m9ShootFX; } 
  SoundFXPtr getM9reloadFX () { return _m9ReloadFX; } 
  SoundFXPtr getM9emptyFX () { return _m9EmptyFX; } 
  SoundFXPtr getM4shootFX () { return _m4ShootFX; } 
  SoundFXPtr getM4reloadFX () { return _m4ReloadFX; } 
  SoundFXPtr getM4emptyFX () { return _m4EmptyFX; } 
  SoundFXPtr getVoiceEnemyDownFX () { return _voiceEnemyDownFX; } 
  SoundFXPtr getVoiceLetsGoFX () { return _voiceLetsgoFX; }  
  SoundFXPtr getVoiceMissionFailedFX () { return _voiceMissionFailedFX; } 
  SoundFXPtr getVoiceMissionCompletedFX () { return _voiceMissionCompletedFX; }   
  SoundFXPtr getTerroristAttackFX () { return _terroristAttack; } 
  
  //SoundFXPtr getSoundFXPtr () { return _simpleEffect; }
  //SoundFXPtr getSoundFXExplosionPtr () { return _explosionEffect; }
  SoundFXPtr getSoundFXShootPtr () { return _shootEffect; }
  SoundFXPtr getSoundFXRechargePtr () { return _rechargeEffect; }
  SoundFXPtr getSoundFXEmptyPtr () { return _emptyEffect; }
  SoundFXPtr getSoundFXIntroStatePtr () { return _introStateEffect; }
  std::string getNamePlayer(){return _namePlayer;}
  void setNamePlayer(std::string pNamePlayer){_namePlayer = pNamePlayer;}
  

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneManager;
  Ogre::RenderWindow* _renderWindow;

  // Funciones de configuración.
  void loadResources ();
  bool configure ();
  bool _initSDL();
  void loadSounds();
  
  // Heredados de FrameListener.
  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

 private:
  // Funciones para delegar eventos de teclado
  // y ratón en el estado actual.
  bool keyPressed (const OIS::KeyEvent &e);
  bool keyReleased (const OIS::KeyEvent &e);

  bool mouseMoved (const OIS::MouseEvent &e);
  bool mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  bool mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  
  // Gestor de eventos de entrada.
  InputManager *_inputMgr;
  // Estados del juego.
  std::stack<GameState*> _states;
};

#endif

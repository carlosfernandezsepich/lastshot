#ifndef SCORE_H
#define SCORE_H

#include <iostream>
#include <string>

using namespace std;

class Score {
public:
    Score();
    Score(int pScore, string pName);
    Score(const Score& orig);
    virtual ~Score();
    void setName( string const &pName);
    
    string getName() const;
    
    void setScore( int const &pScore );
    
    int getScore() const;
    
private:
    
    string _name;
    int _score;
};

#endif /* SCORE_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SoundFXManager.h
 * Author: esi
 *
 * Created on 15 de febrero de 2017, 16:52
 */

#ifndef SOUNDFXMANAGER_H
#define SOUNDFXMANAGER_H

#include <OGRE/Ogre.h>
#include "SoundFX.h"

// Clase encargada de gestionar recursos del tipo "SoundFX".
// Funcionalidad heredada de Ogre::ResourceManager
// y Ogre::Singleton.
class SoundFXManager: public Ogre::ResourceManager,
                      public Ogre::Singleton<SoundFXManager> {
 public:
  SoundFXManager();
  virtual ~SoundFXManager();

  virtual SoundFXPtr load(const Ogre::String& name,
			  const Ogre::String& group = \
			  Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

  static SoundFXManager& getSingleton();
  static SoundFXManager* getSingletonPtr();

  static int getAvailableChannels();
  
 protected:
  Ogre::Resource* createImpl(const Ogre::String& name, Ogre::ResourceHandle handle,
			     const Ogre::String& group, bool isManual,
			     Ogre::ManualResourceLoader* loader,
			     const Ogre::NameValuePairList* createParams);
    
 private:
  static int _numChannels;
};

#endif /* SOUNDFXMANAGER_H */


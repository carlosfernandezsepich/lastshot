/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TrackManager.h
 * Author: esi
 *
 * Created on 15 de febrero de 2017, 16:50
 */

#ifndef TRACKMANAGER_H
#define TRACKMANAGER_H

#include <OGRE/Ogre.h>
#include <Track.h>

// Clase encargada de gestionar recursos del tipo "Track".
// Funcionalidad heredada de Ogre::ResourceManager
// y Ogre::Singleton.
class TrackManager: public Ogre::ResourceManager,
                    public Ogre::Singleton<TrackManager> {
 public:
  TrackManager();
  virtual ~TrackManager();
  virtual TrackPtr load (const Ogre::String& name,
			 const Ogre::String& group = \
			 Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
  static TrackManager& getSingleton ();
  static TrackManager* getSingletonPtr ();
  
 protected:
  Ogre::Resource* createImpl (const Ogre::String& name, Ogre::ResourceHandle handle,
			      const Ogre::String& group, bool isManual,
			      Ogre::ManualResourceLoader* loader,
			      const Ogre::NameValuePairList* createParams);
};

#endif /* TRACKMANAGER_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Player.h
 * Author: marcado
 *
 * Created on 14 de junio de 2017, 20:10
 */

#ifndef PLAYER_H
#define PLAYER_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "TrackManager.h"
#include "SoundFXManager.h"

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"		
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBulletCollisionsRay.h"

#include <OgreOverlaySystem.h>
#include <OgreOverlayElement.h>
#include <OgreOverlayManager.h>
#include <OgreFontManager.h>
#include <OgreTextAreaOverlayElement.h>

using namespace Ogre;

class Player {
public:
    Player(SceneManager *sceneMgr, Camera *myCam, OgreBulletDynamics::DynamicsWorld *pWorld, Ogre::Viewport *pViewport);
    Player(const Player& orig);
    virtual ~Player();
    
    void update (Real elapsedTime, Ogre::Vector2 mRot,  OIS::KeyCode key, OIS::MouseButtonID mouse);
         // The three methods below returns the two camera-related nodes, 
         // and the current position of the character (for the 1st person camera)
    SceneNode *getCameraNode () {
             return nodeCamera;
         }
    SceneNode *getPlayerNode () {
             return nodePlayer;
         }
    Vector3 getWorldPosition () {
             return nodePlayer->_getDerivedPosition ();
         }
    OgreBulletDynamics::RigidBody* getRigidBody () {
             return rigidBody;
         }
    OgreBulletCollisions::BoxCollisionShape* getCollisionShape () {
             return shape;
         }
    void setLife(int pLife) {life = pLife;}
  //  int getLife() {return life;}
 //   void overlayHerido();
 //   void setOpacityOverlay(int pValor);
    int decideHurt(Ogre::Real y);
    void deadPlayer();
    void loadAnimations();
    void updateAnimations();
    void moveW();
    void moveS();
    void moveA();
    void moveD();
    void stop();
    void shot();
    void reload();
    bool checkShoot();
    void reduce(Ogre::int32 i);
    Ogre::int32 getLife(){return test;}
    void overlayHerido();
    
         
private:
     SceneNode *nodeParche; 
     SceneNode *nodePlayer; 
     SceneNode *nodeCamera; 
     SceneNode *nodeArm;
     Camera *camera;
     int life;
     Ogre::Real timeDead;
     Ogre::int32 test;
     Ogre::Real deltaT;
     Ogre::Real timeBlood;
     Ogre::OverlayContainer* _panelP;
     Ogre::Overlay *_overlay;
     OgreBulletDynamics::DynamicsWorld *world;
     OgreBulletDynamics::RigidBody *rigidBody;
     OgreBulletCollisions::BoxCollisionShape *shape;
     Ogre::Viewport *viewport;
     Ogre::Entity *entM4;
     Ogre::Entity *entM4Cock;  
     Ogre::Entity *entM4Magazine; 
     Ogre::Entity *entM9;
     Ogre::Entity *entM92;
     Ogre::Entity *entM9Cargador;
     Ogre::Entity *entBody; 
     Ogre::Entity *entM9Gatillo; 
     SceneManager *nodeSceneMgr;
       //-----ANIMATIONS UP-----------  
     Ogre::AnimationState *_AnimM4UP;
     Ogre::AnimationState *_AnimM4CockUP;
     Ogre::AnimationState *_AnimM4MagazineUP;
     Ogre::AnimationState *_AnimBodyM4UP; 
     Ogre::AnimationState *_AnimM9UP;
     Ogre::AnimationState *_AnimM9_2UP;
     Ogre::AnimationState *_AnimM9GatilloUP;
     Ogre::AnimationState *_AnimM9CargadorUP;
     Ogre::AnimationState *_AnimBodyM9UP;  
  
  //-----ANIMATIONS DOWN-----------  
     Ogre::AnimationState *_AnimM4Down;
     Ogre::AnimationState *_AnimM4CockDown;
     Ogre::AnimationState *_AnimM4MagazineDown;  
     Ogre::AnimationState *_AnimBodyM4Down; 
     Ogre::AnimationState *_AnimM9Down;
     Ogre::AnimationState *_AnimM9_2Down;
     Ogre::AnimationState *_AnimM9GatilloDown;
     Ogre::AnimationState *_AnimM9CargadorDown;
     Ogre::AnimationState *_AnimBodyM9Down;  
  
  //-----ANIMATIONS SHOOT-----------
     Ogre::AnimationState *_AnimM9Shoot;
     Ogre::AnimationState *_AnimM9_2Shoot;
     Ogre::AnimationState *_AnimM9GatilloShoot;
     Ogre::AnimationState *_AnimM9CargadorShoot;
     Ogre::AnimationState *_AnimBodyM9Shoot;  
     Ogre::AnimationState *_AnimM4Shoot;
     Ogre::AnimationState *_AnimM4CockShoot;
     Ogre::AnimationState *_AnimM4MagazineShoot;
     Ogre::AnimationState *_AnimBodyM4Shoot; 
  
     //-----ANIMATIONS RELOAD---------
     Ogre::AnimationState *_AnimM9Reload;
     Ogre::AnimationState *_AnimM9_2Reload;
     Ogre::AnimationState *_AnimM9GatilloReload;
     Ogre::AnimationState *_AnimM9CargadorReload;
     Ogre::AnimationState *_AnimBodyM9Reload;
     Ogre::AnimationState *_AnimM4Reload;
     Ogre::AnimationState *_AnimM4CockReload;
     Ogre::AnimationState *_AnimM4MagazineReload;
     Ogre::AnimationState *_AnimBodyM4Reload;
     
     Vector3 directionDead;
};

#endif /* PLAYER_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   IA.h
 * Author: marcado
 *
 * Created on 9 de junio de 2017, 22:05
 */

#ifndef IA_H
#define IA_H

#include "Terrorist.h"
#include "Weapon.h"
#include <string>
#include <iostream> 
#include <vector>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"		
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBulletCollisionsRay.h"


using namespace Ogre;
using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;


class IA {
public:
    IA(std::vector<Terrorist*> *pTerrorists, RigidBody *pPlayer, OgreBulletDynamics::DynamicsWorld *pWorld, Weapon *pWeapon);
    IA(const IA& orig);
    virtual ~IA();
    void updateWorld(Ogre::Real pDeltaT);
private:

//    int patrullando, rastreando, atacando, muerto;
    bool notifyAllTerrorists;
    Ogre::Real deltaT;
    Ogre::Real auxT;
    RigidBody *player;
    std::vector<Terrorist*> *terrorists;
    OgreBulletDynamics::DynamicsWorld *world;
    Weapon *weapon;
    int limitCharger;
    int limitTotal;
};

#endif /* IA_H */


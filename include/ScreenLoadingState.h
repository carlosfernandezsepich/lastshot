#ifndef ScreenLoadingState_H
#define ScreenLoadingState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include "TrackManager.h"
#include "SoundFXManager.h"
#include <OgreOverlaySystem.h>
#include <OgreOverlayElement.h>
#include <OgreOverlayManager.h>
#include <OgreFontManager.h>
#include <OgreTextAreaOverlayElement.h>

#include "GameState.h"

class ScreenLoadingState : public Ogre::Singleton<ScreenLoadingState>, public GameState
{
 public:
  ScreenLoadingState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static ScreenLoadingState& getSingleton ();
  static ScreenLoadingState* getSingletonPtr ();
  
  CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  CEGUI::Window* _sheet;
  Ogre::OverlayContainer* _panel;
  Ogre::Real timeFirstImage;
  Ogre::Real timeSecondImage;
  Ogre::Real _deltaT;
  

  bool _exitGame;
  void createWallPaper();
  void changeWallPaper();
  void deleteWallPaper();
  void changeStateGame();
};

#endif

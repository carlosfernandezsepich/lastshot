#ifndef MenuState_H
#define MenuState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <string>
#include "TrackManager.h"
#include "SoundFXManager.h"

#include "GameState.h"

#include <OgreOverlaySystem.h>
#include <OgreOverlayElement.h>
#include <OgreOverlayManager.h>
#include <OgreFontManager.h>
#include <OgreTextAreaOverlayElement.h>

#include <iostream>       // std::cout
#include <thread>         // std::thread, std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>
 
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/generator_iterator.hpp>


class MenuState : public Ogre::Singleton<MenuState>, public GameState
{
 public:
  MenuState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static MenuState& getSingleton ();
  static MenuState* getSingletonPtr ();
  
  void createMenu();
  void deleteMenu();
  void createWallPaper();
  void createScreenLoading();
  static void ex();
  bool play(const CEGUI::EventArgs &e);
  bool score(const CEGUI::EventArgs &e);
  bool credits(const CEGUI::EventArgs &e);
  bool configuration(const CEGUI::EventArgs &e);
  bool quit(const CEGUI::EventArgs &e);
  bool continueGame(const CEGUI::EventArgs &e);
  CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
  void menu1(); 
  void menu2(int n); 
  void menu3(int n); 
  
  void createConfigurationMenu();
  void createCreditsMenu();
  bool returnToMenu(const CEGUI::EventArgs &e);
 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  CEGUI::Window* _sheet;
  std::vector<std::string> _myVector;
  CEGUI::Window* editBoxName;
  Ogre::OverlayContainer* _panel;

  bool _exitGame;
  
  void createMenuScore();
  void createWriteName();
};

#endif

#include "ScreenLoadingState.h"
#include "PlayState.h"
#include "MenuState.h"
//#include "Score.h"
#include <string>
#include <stdlib.h>

#include <OGRE/Overlay/OgreOverlayElement.h>

#include <thread> // std::thread, std::this_thread::sleep_for
#include <chrono> // std::chrono::seconds


using namespace std;
template<> ScreenLoadingState* Ogre::Singleton<ScreenLoadingState>::msSingleton = 0;

void
ScreenLoadingState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _root->getAutoCreatedWindow()->removeAllViewports();
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  double width = _viewport->getActualWidth();
  double height = _viewport->getActualHeight();
  _camera->setAspectRatio(width / height);
  // Nuevo background colour.
  _viewport->setBackgroundColour(Ogre::ColourValue(0.5, 0.5, 0.5));
  OverlayManager::getSingletonPtr()->destroyAllOverlayElements();
  OverlayManager::getSingletonPtr()->destroyAll();
  GameManager::getSingletonPtr()->getMenuStateTrack()->play();     //Play main music

  //_viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 1.0));
  _exitGame = false;
  timeFirstImage = 6.0;
  timeSecondImage = 6.5;
  createWallPaper();
  //GameManager::getSingletonPtr()->_shootEffect->play();
  //  GameManager::getSingletonPtr()->   //Play main music
  


}

void
ScreenLoadingState::exit()
{
  Ogre::OverlayManager::getSingletonPtr()->destroyAll();  
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();
}

void
ScreenLoadingState::pause ()
{
}

void
ScreenLoadingState::resume ()
{
}

bool
ScreenLoadingState::frameStarted
(const Ogre::FrameEvent& evt) 
{
  _deltaT = evt.timeSinceLastFrame;  
  timeFirstImage -= _deltaT;
  timeSecondImage -= _deltaT;
  if(timeFirstImage <= 0.0) changeWallPaper();
  if(timeSecondImage <= 0.0) changeStateGame();
  CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(evt.timeSinceLastFrame);
    
  return true;
}

bool
ScreenLoadingState::frameEnded
(const Ogre::FrameEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(evt.timeSinceLastFrame);  
  if (_exitGame)
    return false;
  
  return true;
}

void
ScreenLoadingState::keyPressed
(const OIS::KeyEvent &e)
{
  // Transición al siguiente estado.
  // Espacio --> PlayState
  if (e.key == OIS::KC_SPACE) {
//      pushState(MenuState::getSingletonPtr());
  }
}

void
ScreenLoadingState::keyReleased
(const OIS::KeyEvent &e )
{
  if (e.key == OIS::KC_ESCAPE) {
    popState();
    changeState(MenuState::getSingletonPtr());
  }
}

void
ScreenLoadingState::mouseMoved
(const OIS::MouseEvent &e)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel); 
}

void
ScreenLoadingState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
 CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void
ScreenLoadingState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton ScreenLoadingState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

ScreenLoadingState*
ScreenLoadingState::getSingletonPtr ()
{
return msSingleton;
}

ScreenLoadingState&
ScreenLoadingState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

void ScreenLoadingState::createWallPaper(){
  
//    OverlayManager::getSingletonPtr()->destroyAllOverlayElements();
//    OverlayManager::getSingletonPtr()->destroyAll();
    //deleteMenu();
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Ogre::Overlay *_overlay = _overlayMgr->create("overlayScreenLoading");
    _panel = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerScreenLoading"));
    _panel->setMetricsMode(Ogre::GMM_PIXELS);
    _panel->setPosition(0, 0);
    _panel->setDimensions(width, height);
    _panel->setMaterialName("ScreenLoading1"); // Optional background material
    _overlay->add2D(_panel);
    
    _overlay->show();
   

  
}
void ScreenLoadingState::changeWallPaper(){

//   OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
//   Ogre::Overlay *_overlay = _overlayMgr->getByName("overlayMenuState");
//   _overlay->getChild("containerMenuState")->setMaterialName("ScreenLoading2");
   _panel->setMaterialName("ScreenLoading2");
  // std::this_thread::sleep_for (std::chrono::seconds(6));
  // changeStateGame();
}
void ScreenLoadingState::changeStateGame(){

    changeState(PlayState::getSingletonPtr());
}

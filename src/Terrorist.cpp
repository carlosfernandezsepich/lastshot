
#include <valarray>

#include "Terrorist.h"
#include "PlayState.h"

Terrorist::Terrorist(int pId, Ogre::Entity *pEntityModel, OgreBulletDynamics::RigidBody *pRigidBody, OgreBulletDynamics::DynamicsWorld *pWorld, Player *pPlayer) {
    _id = pId;
//    _state = 0; //Patrullando
    _entityModel = pEntityModel;
    _rigidBody = pRigidBody;
    world = pWorld;
    _bullets = 0;
    directionRotation = "right";
//    life = 100;
    test = 100;
    _timeNoWatchPlayer = 0;
    _exitGame = false;


//    _timeEvitarColli = 1.0;//    playerDetected = false;
//    _timeNoDetectedPlayer = 0;
//    activatedHerido = false;
    player = pPlayer;
//    overlayPlayer = false;
//    typeAtack = 0; //No seleccionado
    rotate("right");


}

Terrorist::Terrorist(const Terrorist& orig) {
}

Terrorist::~Terrorist() {
}
void Terrorist::loadAnimations(){
    
  //---------------------------------RUN------------------------------------------------------
  _AnimEnemyRunModel = _entityModel->getAnimationState("bot_run");
  //-------------------------------WALK--------------------------------------------------------
  _AnimEnemyWalkModel = _entityModel->getAnimationState("bot_walk"); 
  //-------------------------------CROUCH------------------------------------------------------
  _AnimEnemyCrouchModel = _entityModel->getAnimationState("bot_crouch"); 
 //-------------------------------IDLE---------------------------------------------------------  
  _AnimEnemyIdleModel = _entityModel->getAnimationState("bot_idle");
 //-------------------------------SHOOT--------------------------------------------------------  
  _AnimEnemyShootModel = _entityModel->getAnimationState("bot_shoot");  
 //-------------------------------RELOAD-------------------------------------------------------  
  _AnimEnemyReloadModel = _entityModel->getAnimationState("bot_reload");  
 //--------------------------------AIM---------------------------------------------------------  
  _AnimEnemyAimModel = _entityModel->getAnimationState("bot_aim"); 
 //-----------------------------SHOOT-CROUCH---------------------------------------------------  
  _AnimEnemyShootCrouchModel = _entityModel->getAnimationState("bot_shootCrouch");    
 //-----------------------------AIM-CROUCH---------------------------------------------------  
  _AnimEnemyAimCrouchModel = _entityModel->getAnimationState("bot_aimCrouch");     
 //---------------------------RELOAD-CROUCH--------------------------------------------------  
  _AnimEnemyReloadCrouchModel = _entityModel->getAnimationState("bot_reloadCrouch");   
 //-------------------------------DEAD--------------------------------------------------  
  _AnimEnemyDeadModel = _entityModel->getAnimationState("bot_die");     
 //-----------------------------DEAD-CROUCH--------------------------------------------------  
  _AnimEnemyDeadCrouchModel = _entityModel->getAnimationState("bot_dieCrouch");     
}
void Terrorist::updateAnimations(Ogre::Real pDeltaT){

    if(!_exitGame){
    _deltaT = pDeltaT;
    UpdateState();
   if (_AnimEnemyRunModel->getEnabled() && !_AnimEnemyRunModel->hasEnded())
    _AnimEnemyRunModel->addTime(pDeltaT);      //Run
   if (_AnimEnemyWalkModel->getEnabled() && !_AnimEnemyWalkModel->hasEnded())
    _AnimEnemyWalkModel->addTime(pDeltaT);     //Walk
   if (_AnimEnemyIdleModel->getEnabled() && !_AnimEnemyIdleModel->hasEnded())
    _AnimEnemyIdleModel->addTime(pDeltaT);     //Idle
   if (_AnimEnemyCrouchModel->getEnabled() && !_AnimEnemyCrouchModel->hasEnded())
    _AnimEnemyCrouchModel->addTime(pDeltaT);   // Crouch
   if (_AnimEnemyShootModel->getEnabled() && !_AnimEnemyShootModel->hasEnded())
    _AnimEnemyShootModel->addTime(pDeltaT);     //Idle
   if (_AnimEnemyShootCrouchModel->getEnabled() && !_AnimEnemyShootCrouchModel->hasEnded())
    _AnimEnemyShootCrouchModel->addTime(pDeltaT);// Shoot Crouch
   if (_AnimEnemyReloadModel->getEnabled() && !_AnimEnemyReloadModel->hasEnded())
    _AnimEnemyReloadModel->addTime(pDeltaT);   // Reload 
   if (_AnimEnemyAimModel->getEnabled() && !_AnimEnemyAimModel->hasEnded())
    _AnimEnemyAimModel->addTime(pDeltaT);   // Aim
   if (_AnimEnemyAimCrouchModel->getEnabled() && !_AnimEnemyAimCrouchModel->hasEnded())
    _AnimEnemyAimCrouchModel->addTime(pDeltaT);   // Aimcrouch  
   if (_AnimEnemyReloadCrouchModel->getEnabled() && !_AnimEnemyReloadCrouchModel->hasEnded())
    _AnimEnemyReloadCrouchModel->addTime(pDeltaT);   // Reload crouch      
   if (_AnimEnemyDeadModel->getEnabled() && !_AnimEnemyDeadModel->hasEnded())
    _AnimEnemyDeadModel->addTime(pDeltaT);   // dead crouch  
   if (_AnimEnemyDeadCrouchModel->getEnabled() && !_AnimEnemyDeadCrouchModel->hasEnded())
    _AnimEnemyDeadCrouchModel->addTime(pDeltaT);   // Dead crouch    

    }
}
    

// FUNCIONES INTERNAS DE LA LOGICA
void Terrorist::run(bool pValor, Ogre::Vector3 pDirection){

    if(pValor && (!_AnimEnemyRunModel->getEnabled() || _AnimEnemyRunModel->hasEnded())){
       _AnimEnemyRunModel->setTimePosition(0);
       _AnimEnemyRunModel->setLoop(true);
       _AnimEnemyRunModel->setEnabled(pValor);
    }else{
       _AnimEnemyRunModel->setEnabled(pValor);
    }
       _rigidBody->setLinearVelocity(pDirection.normalisedCopy() * 66.0 * _deltaT);
    
}

Vector3 Terrorist::createRay(){

        btRigidBody *rigidBody = _rigidBody->getBulletRigidBody();
        Ogre::Ray r(OgreBulletCollisions::convert(rigidBody->getCenterOfMassPosition()), OgreBulletCollisions::convert(rigidBody->getOrientation()) * Ogre::Vector3::UNIT_Z);
        OgreBulletCollisions::CollisionClosestRayResultCallback cQuery = CollisionClosestRayResultCallback (r, world, 3);
        world->launchRay(cQuery);                            
        Vector3 normalisedDirection = r.getDirection();
        normalisedDirection.y = 0;
        normalisedDirection.normalisedCopy();
        return normalisedDirection;
                    
}
void Terrorist::moveW(bool pValor, Ogre::Vector3 pDirection){

     if(pValor && (!_AnimEnemyWalkModel->getEnabled() || _AnimEnemyWalkModel->hasEnded())){
       _AnimEnemyWalkModel->setTimePosition(0);
       _AnimEnemyWalkModel->setLoop(true);
       _AnimEnemyWalkModel->setEnabled(pValor);
    }else{
       _AnimEnemyWalkModel->setEnabled(pValor);

    }
    
       _rigidBody->setLinearVelocity(pDirection.normalisedCopy() * 60.0 * _deltaT);
}
void Terrorist::moveS(bool pValor, Ogre::Vector3 pDirection){

       _AnimEnemyWalkModel->setTimePosition(0.1);
       _AnimEnemyWalkModel->setLoop(true);
       _AnimEnemyWalkModel->setEnabled(pValor);
       
       _rigidBody->setLinearVelocity(pDirection.normalisedCopy() * -6.5);
}

void Terrorist::moveA(bool pValor, Ogre::Vector3 pDirection){

       _AnimEnemyWalkModel->setTimePosition(0.1);
       _AnimEnemyWalkModel->setLoop(true);
       _AnimEnemyWalkModel->setEnabled(pValor);
       pDirection.y = 0;
       pDirection = pDirection.crossProduct( Vector3::UNIT_Y );
       pDirection = pDirection.normalisedCopy();
       _rigidBody->setLinearVelocity(pDirection * 6.5);
}

void Terrorist::moveD(bool pValor, Ogre::Vector3 pDirection){

       _AnimEnemyWalkModel->setTimePosition(0.1);
       _AnimEnemyWalkModel->setLoop(true);
       _AnimEnemyWalkModel->setEnabled(pValor);
       
       pDirection.y = 0;
       pDirection = pDirection.crossProduct( Vector3::NEGATIVE_UNIT_Y );
       pDirection = pDirection.normalisedCopy();
       _rigidBody->setLinearVelocity(pDirection * 6.5);
}

void Terrorist::stop(){

    Vector3 direction(0,0,0);
        _rigidBody->setLinearVelocity(direction * 0);  
}

void Terrorist::crouch(bool pValor){

     if(pValor && (!_AnimEnemyCrouchModel->getEnabled() || _AnimEnemyCrouchModel->hasEnded())){
       _AnimEnemyCrouchModel->setTimePosition(0.1);
       _AnimEnemyCrouchModel->setLoop(true);
       _AnimEnemyCrouchModel->setEnabled(pValor);
    }else{
       _AnimEnemyCrouchModel->setEnabled(pValor);
    }
       
       GameManager::getSingletonPtr()->getSoundFXShootPtr()->play();
}

void Terrorist::rotate(std::string pType){

    if(pType == "stop") _rigidBody->getBulletRigidBody()->setAngularVelocity(btVector3(0,0,0));
    else if(pType == "left") {//_rigidBody->setAngularVelocity(0,-2.5,0);
    btTransform btT;
    _rigidBody->getBulletRigidBody()->getMotionState()->getWorldTransform(btT);
    //------------------NEW ROTATION-------------------
    btQuaternion btNewQuaternion;
    btNewQuaternion.setEuler(btScalar(0.1 * 18.0 * 0.010), btScalar(0), btScalar(0));
    //-------------------------------------------------
    btT.setRotation(btNewQuaternion * _rigidBody->getBulletRigidBody()->getOrientation());
    _rigidBody->getBulletRigidBody()->setCenterOfMassTransform(btT);
    _rigidBody->getSceneNode()->_setDerivedOrientation(_rigidBody->getCenterOfMassOrientation());
    }//_rigidBody->getBulletRigidBody()->setAngularVelocity(btVector3(0,-2.5,0));
    else {
    {//_rigidBody->setAngularVelocity(0,-2.5,0);
    btTransform btT;
    _rigidBody->getBulletRigidBody()->getMotionState()->getWorldTransform(btT);
    //------------------NEW ROTATION-------------------
    btQuaternion btNewQuaternion;
    btNewQuaternion.setEuler(btScalar(-0.1 * 18.0 * 0.010), btScalar(0), btScalar(0));
    //-------------------------------------------------
    btT.setRotation(btNewQuaternion * _rigidBody->getBulletRigidBody()->getOrientation());
    _rigidBody->getBulletRigidBody()->setCenterOfMassTransform(btT);
    }//_rigidBody->getBulletRigidBody()->setAngularVelocity(btVector3(0,-2.5,0));
    }
    
}

//FUNCIONES MIEMBRO

void Terrorist::Inicializar(void) {
  CurrentState=vigilando;
  loadAnimations();
}

StateType Terrorist::Informar(void) {
  return CurrentState;
}

void Terrorist::ChangeState(StateType State) {
    CurrentState=State;
}

void Terrorist::ChangeState(char State){
    
    
    switch(State)
    {
        case 'v': 
        CurrentState = vigilando;
        _timeNoWatchPlayer = 0;
        break;
        case 'a': 
            if(CurrentState == vigilando){
               _AnimEnemyIdleModel->setTimePosition(0);
               _AnimEnemyIdleModel->setLoop(false);
               _AnimEnemyIdleModel->setEnabled(false);
            } //Pregunto por el estado actual
        CurrentState = atentando; //Cambio de estado

        
        break;
        case 'h': CurrentState = huyendo;
        break;
        case 'm': CurrentState = muerto;
        break;
    }
}

void Terrorist::UpdateState(void) {
    switch(CurrentState) {
  case vigilando:
      std::cout<<"Terrorista "<<_id<<" esta "<<"vigilando"<<std::endl;
      _rigidBody->enableActiveState();
      if(!_AnimEnemyIdleModel->getEnabled() || _AnimEnemyIdleModel->hasEnded())
      {
           _AnimEnemyIdleModel->setTimePosition(0);
           _AnimEnemyIdleModel->setLoop(false);
           _AnimEnemyIdleModel->setEnabled(true);
        
      }
      if(_id % 2 == 0) //Los enemigos con ID par tambien se mueven cuando vigilan
      {
          
          _rigidBody->enableActiveState();
          Ogre::Ray r(_rigidBody->getCenterOfMassPosition(), OgreBulletCollisions::convert(_rigidBody->getBulletRigidBody()->getOrientation()) * Ogre::Vector3::UNIT_Z);
          OgreBulletCollisions::CollisionClosestRayResultCallback cQuery = CollisionClosestRayResultCallback (r, world, 10);
          world->launchRay(cQuery);
            if (!cQuery.doesCollide()) 
            { //Si el rayo NO detecta una colision
                moveW(true, r.getDirection());
            }
            else rotate("left");
      }

    break;

  case atentando:
  {
      std::cout<<"Terrorista "<<_id<<" esta "<<"atacando"<<std::endl;
      _rigidBody->enableActiveState();
      _timeShoot+=_deltaT;
        if(_id % 2 == 0) shootNormal();       //Parado 
        else shootProfessional();             //Agachado
  }
    break;
       
    
  case huyendo:
  {
      std::cout<<"Terrorista "<<_id<<" esta "<<"huyendo"<<std::endl;
      _rigidBody->enableActiveState();
      Ogre::Ray r(_rigidBody->getCenterOfMassPosition(), OgreBulletCollisions::convert(_rigidBody->getBulletRigidBody()->getOrientation()) * Ogre::Vector3::UNIT_Z);
      OgreBulletCollisions::CollisionClosestRayResultCallback cQuery = CollisionClosestRayResultCallback (r, world, 10);
      world->launchRay(cQuery);
      if (!cQuery.doesCollide()) 
        { //Si el rayo NO detecta una colision
            run(true, r.getDirection());
        }
      else rotate("left");
  }
      
      
    break;
    
      case muerto:
      {
        _rigidBody->enableActiveState();  
        if(_id % 2 == 0) dead();      
        else deadCrouch(); 
      }
          break;
          
  }
}

//ACCIONES

void Terrorist::shootNormal(){
    
    bool detected = false;

     if(!_AnimEnemyAimModel->getEnabled() && !_AnimEnemyReloadModel->getEnabled()) 
          {
              _AnimEnemyAimModel->setTimePosition(0);
              _AnimEnemyAimModel->setLoop(false);
              _AnimEnemyAimModel->setEnabled(true);
            
          }
     if(_AnimEnemyReloadModel->getEnabled() && _AnimEnemyReloadModel->hasEnded())
          {
            _AnimEnemyReloadModel->setEnabled(false);
          }
     if(_AnimEnemyAimModel->getEnabled() && _AnimEnemyAimModel->hasEnded())
          {
             if(_bullets>=30)
                    {
                        //Desactivo animacion anterior 
                        _AnimEnemyAimModel->setEnabled(false);
                        _AnimEnemyReloadModel->setEnabled(false); 
                        reload(); 
                    }
                    else
                    {
                     Ogre::Vector3 pos = OgreBulletCollisions::convert(_rigidBody->getBulletRigidBody()->getCenterOfMassPosition());
    Ogre::Ray rShoot(pos, OgreBulletCollisions::convert(_rigidBody->getBulletRigidBody()->getOrientation()) * Ogre::Vector3::UNIT_Z);
                          OgreBulletCollisions::CollisionClosestRayResultCallback cQueryShoot = CollisionClosestRayResultCallback (rShoot, world, 100);
                          world->launchRay(cQueryShoot);
                           if (cQueryShoot.doesCollide()) 
                            { //Si el rayo detecta una colision
                                if(cQueryShoot.getCollidedObject()->getName() == "rigidBodyPlayer") {detected = true; _timeNoWatchPlayer=0;}
   
                                else {detected = false; _timeNoWatchPlayer+=_deltaT;}
                            }
    
    
        if(detected){
            rotate("stop");
                       if(_timeShoot>=0.3)
                       {
                          _AnimEnemyShootModel->setTimePosition(0);
                          _AnimEnemyShootModel->setLoop(false);
                          _AnimEnemyShootModel->setEnabled(true); 
                          _bullets+=1; _timeShoot = 0;
                          GameManager::getSingletonPtr()->_akShootFX->play();
                          btRigidBody *rigidBody = _rigidBody->getBulletRigidBody();
                          Ogre::Vector3 position = OgreBulletCollisions::convert(rigidBody->getCenterOfMassPosition());
                          Ogre::Vector3 positionleft, positionright, positiontop;
                          positionleft = position; positionright = position; positiontop = position;
                          positionleft.x-=20; positionright+=20; positiontop.y+=20;
                          int num = 1 + rand() % (3 - 1);
                          switch(num)
                          {  
                            case 1: position = position;
                                break;
                            case 2: position = positionleft;
                                break;
                            case 3: position = positionright;
                                break;
                            case 4: position = positiontop;
                                break;
                          }
                          Ogre::Ray rShoot(position, OgreBulletCollisions::convert(rigidBody->getOrientation()) * Ogre::Vector3::UNIT_Z);
                          OgreBulletCollisions::CollisionClosestRayResultCallback cQueryShoot = CollisionClosestRayResultCallback (rShoot, world, 100);
                          world->launchRay(cQueryShoot);
                           if (cQueryShoot.doesCollide()) 
                            { //Si el rayo detecta una colision
                                if(cQueryShoot.getCollidedObject()->getName() == "rigidBodyPlayer") //Pregunto si es el player
                                    {  //El enemigo me ha dado                                 
                                      player->reduce(Ogre::int32(player->decideHurt(rShoot.getDirection().y)));  

                                    } 
                            }
                        }
        } else{
        
            if(key == OIS::KC_D) {directionRotation = "left";}
            else if(key == OIS::KC_A) {directionRotation = "right";}
            else {directionRotation = "left";}
            rotate("stop");
            rotate(directionRotation);
        } 
            
                
                    }  
                         
                    
     }

          
}

void Terrorist::shootProfessional(){
 
    bool detected = false;
            
     if(!_AnimEnemyCrouchModel->getEnabled() && !_AnimEnemyAimCrouchModel->getEnabled() && !_AnimEnemyShootModel->getEnabled()) 
          {
              _AnimEnemyCrouchModel->setTimePosition(0);
              _AnimEnemyCrouchModel->setLoop(false);
              _AnimEnemyCrouchModel->setEnabled(true);            
          }
     if(_AnimEnemyCrouchModel->getEnabled() && _AnimEnemyCrouchModel->hasEnded())
          {
              _AnimEnemyCrouchModel->setEnabled(false);
              _AnimEnemyAimCrouchModel->setTimePosition(0);
              _AnimEnemyAimCrouchModel->setLoop(false);
              _AnimEnemyAimCrouchModel->setEnabled(true); 
          }
     if(_AnimEnemyAimCrouchModel->getEnabled() && _AnimEnemyAimCrouchModel->hasEnded())
          {
             if(_bullets>=30)
                    {
                        //Desactivo animacion anterior 
                        _AnimEnemyCrouchModel->setEnabled(false);
                        _AnimEnemyAimCrouchModel->setEnabled(false);
                        _AnimEnemyShootCrouchModel->setEnabled(false);
                        reload(); 
                    }
                    else
                    {
                        
    Ogre::Vector3 pos = OgreBulletCollisions::convert(_rigidBody->getBulletRigidBody()->getCenterOfMassPosition());
    Ogre::Ray rShoot(pos, OgreBulletCollisions::convert(_rigidBody->getBulletRigidBody()->getOrientation()) * Ogre::Vector3::UNIT_Z);
                          OgreBulletCollisions::CollisionClosestRayResultCallback cQueryShoot = CollisionClosestRayResultCallback (rShoot, world, 100);
                          world->launchRay(cQueryShoot);
                           if (cQueryShoot.doesCollide()) 
                            { //Si el rayo detecta una colision
                                if(cQueryShoot.getCollidedObject()->getName() == "rigidBodyPlayer") {detected = true; _timeNoWatchPlayer=0;}
   
                                else {detected = false; _timeNoWatchPlayer+=_deltaT;}
                            }
    
    
        if(detected){
            rotate("stop");
                       if(_timeShoot>=0.3)
                       {
                          _AnimEnemyShootCrouchModel->setTimePosition(0);
                          _AnimEnemyShootCrouchModel->setLoop(false);
                          _AnimEnemyShootCrouchModel->setEnabled(true); 
                          _bullets+=1; _timeShoot = 0;
                          GameManager::getSingletonPtr()->_akShootFX->play();   
                          btRigidBody *rigidBody = _rigidBody->getBulletRigidBody();
                          Ogre::Vector3 position = OgreBulletCollisions::convert(rigidBody->getCenterOfMassPosition());
                          Ogre::Vector3 positionleft, positionright, positiontop;
                          positionleft = position; positionright = position; positiontop = position;
                          positionleft.x-=20; positionright+=20; positiontop.y+=20;
                          int num = 1 + rand() % (2 - 1);
                          switch(num)
                          {  
                            case 1: position = position;
                                break;
                            case 2: position = positionleft;
                                break;
                            case 3: position = positionright;
                                break;
                            case 4: position = positiontop;
                                break;
                          }
                          Ogre::Ray rShoot(position, OgreBulletCollisions::convert(rigidBody->getOrientation()) * Ogre::Vector3::UNIT_Z);
                          OgreBulletCollisions::CollisionClosestRayResultCallback cQueryShoot = CollisionClosestRayResultCallback (rShoot, world, 100);
                          world->launchRay(cQueryShoot);
                           if (cQueryShoot.doesCollide()) 
                            { //Si el rayo detecta una colision
                                if(cQueryShoot.getCollidedObject()->getName() == "rigidBodyPlayer") //Pregunto si es el player
                                    {  //El enemigo me ha dado                                 
                                    //    player->setLife(90);
                                    player->reduce(Ogre::int32(player->decideHurt(rShoot.getDirection().y)));  
                                   // player->setDirectionDead(rShoot.getDirection());
                                   // player->setLife(i-1);
                                    } 
                            }
                        }
        }else{
        
            if(key == OIS::KC_D) {directionRotation = "left";}
            else if(key == OIS::KC_A) {directionRotation = "right";}
            else {directionRotation = "left";}
            rotate("stop");
            rotate(directionRotation);
            
        } 
                
                    }
          }
       // } else rotate("right");
}
void Terrorist::dead(){


    if(!_AnimEnemyDeadModel->getEnabled()){
        std::cout<<"Muerto: "<<_rigidBody->getName()<<std::endl;
  //   _rigidBody->getBulletRigidBody()->setCollisionFlags(_rigidBody->getBulletRigidBody()->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
    _AnimEnemyDeadModel->setTimePosition(0);
    _AnimEnemyDeadModel->setLoop(false);
    _AnimEnemyDeadModel->setEnabled(true); }
}

void Terrorist::deadCrouch(){

        if(!_AnimEnemyDeadCrouchModel->getEnabled()){
            std::cout<<"Muerto: "<<_rigidBody->getName()<<std::endl; 
   //  _rigidBody->getBulletRigidBody()->setCollisionFlags(_rigidBody->getBulletRigidBody()->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);    _AnimEnemyDeadCrouchModel->setTimePosition(0);
    _AnimEnemyDeadCrouchModel->setLoop(false);
    _AnimEnemyDeadCrouchModel->setEnabled(true); }
}


void Terrorist::shoot(){
        if(!_AnimEnemyShootModel->getEnabled() || _AnimEnemyShootModel->hasEnded()){
//            atacando();
        } 
    if(_bullets < 30) //Tiene balas
    {
        _timeShoot+=_deltaT;
        if(_timeShoot >=0.20)
        {
        _bullets+=1;
        _timeShoot = 0.0;                  
        GameManager::getSingletonPtr()->_akShootFX->play();            
        _AnimEnemyShootModel->setTimePosition(2);
        _AnimEnemyShootModel->setLoop(false);
        _AnimEnemyShootModel->setEnabled(true); 
        //Rayo
                    
        btRigidBody *rigidBody = _rigidBody->getBulletRigidBody();
        Ogre::Vector3 position = OgreBulletCollisions::convert(rigidBody->getCenterOfMassPosition());
        Ogre::Vector3 positionleft, positionright, positiontop;
        positionleft = position; positionright = position; positiontop = position;
        positionleft.x-=20; positionright+=20; positiontop.y+=20;
        int num = 1 + rand() % (2 - 1);
        switch(num){
        
            case 1: position = position;
            break;
            case 2: position = positionleft;
            break;
            case 3: position = positionright;
            break;
            case 4: position = positiontop;
            break;
        }
        Ogre::Ray rShoot(position, OgreBulletCollisions::convert(rigidBody->getOrientation()) * Ogre::Vector3::UNIT_Z);
        OgreBulletCollisions::CollisionClosestRayResultCallback cQueryShoot = CollisionClosestRayResultCallback (rShoot, world, 80);
        world->launchRay(cQueryShoot);
            if (cQueryShoot.doesCollide()) 
            { //Si el rayo detecta una colision
                if(cQueryShoot.getCollidedObject()->getName() == "rigidBodyPlayer") //Pregunto si es el player
                {
                    //El enemigo me ha dado
                    player->reduce(Ogre::int32(player->decideHurt(rShoot.getDirection().y)));  
                   // player->setDirectionDead(rShoot.getDirection().normalisedCopy());        
                } 
                        
            }
        }
    }
        
        
    else{reload();}
}
void Terrorist::reload(){
    if(_id % 2 == 0){
    _AnimEnemyReloadModel->setTimePosition(0);
    _AnimEnemyReloadModel->setLoop(false);
    _AnimEnemyReloadModel->setEnabled(true); 
    }
    else{
    _AnimEnemyReloadCrouchModel->setTimePosition(0);
    _AnimEnemyReloadCrouchModel->setLoop(false);
    _AnimEnemyReloadCrouchModel->setEnabled(true); 
    
    }
    _bullets = 0;
    _timeShoot = -2.0;
    GameManager::getSingletonPtr()->_rechargeEffect->play();

}

void Terrorist::changeStateAtackToVigilandoNormal(){

        _AnimEnemyAimModel->setTimePosition(0);   // Aim
        _AnimEnemyAimModel->setEnabled(false);   // Aim
}

void Terrorist::changeStateAtackToVigilandoProf(){

    if(_AnimEnemyAimCrouchModel->getEnabled()){
        _AnimEnemyAimCrouchModel->setTimePosition(0);   // Aim
        _AnimEnemyAimCrouchModel->setEnabled(false);   // Aim
    }
    if(_AnimEnemyCrouchModel->getEnabled()){
        _AnimEnemyCrouchModel->setTimePosition(0);   // Crouch
        _AnimEnemyCrouchModel->setEnabled(false);  // Crouch
    }


}
void Terrorist::changeStateVigilandoToAtackProf(){

        _AnimEnemyIdleModel->setTimePosition(0);   // Idle
        _AnimEnemyIdleModel->setEnabled(false);   // Idle
}
void Terrorist::changeStateToHuyendoAnims(){

    _AnimEnemyRunModel->setTimePosition(0);     //run
    _AnimEnemyWalkModel->setTimePosition(0);     //Walk
    _AnimEnemyIdleModel->setTimePosition(0);     //Idle
    _AnimEnemyCrouchModel->setTimePosition(0);   // Crouch
    _AnimEnemyShootModel->setTimePosition(0);     //Idle
    _AnimEnemyShootCrouchModel->setTimePosition(0);// Shoot Crouch
    _AnimEnemyReloadModel->setTimePosition(0);   // Reload 
    _AnimEnemyAimModel->setTimePosition(0);   // Aim
    _AnimEnemyAimCrouchModel->setTimePosition(0);   // Aimcrouch  
    _AnimEnemyReloadCrouchModel->setTimePosition(0);   // Reload crouch         
    _AnimEnemyDeadModel->setTimePosition(0);   // dead crouch  
    
    _AnimEnemyRunModel->setEnabled(false);     //run    
    _AnimEnemyWalkModel->setEnabled(false);     //Walk
    _AnimEnemyIdleModel->setEnabled(false);     //Idle
    _AnimEnemyCrouchModel->setEnabled(false);   // Crouch
    _AnimEnemyShootModel->setEnabled(false);     //Idle
    _AnimEnemyShootCrouchModel->setEnabled(false);// Shoot Crouch
    _AnimEnemyReloadModel->setEnabled(false);   // Reload 
    _AnimEnemyAimModel->setEnabled(false);   // Aim
    _AnimEnemyAimCrouchModel->setEnabled(false);   // Aimcrouch  
    _AnimEnemyReloadCrouchModel->setEnabled(false);   // Reload crouch      
    _AnimEnemyDeadModel->setEnabled(false);   // dead crouch  
    
    
    

    
}

void Terrorist::stopAllAnimations(){

    _AnimEnemyRunModel->setTimePosition(0);     //run
    _AnimEnemyWalkModel->setTimePosition(0);     //Walk
    _AnimEnemyIdleModel->setTimePosition(0);     //Idle
    _AnimEnemyCrouchModel->setTimePosition(0);   // Crouch
    _AnimEnemyShootModel->setTimePosition(0);     //Idle
    _AnimEnemyShootCrouchModel->setTimePosition(0);// Shoot Crouch
    _AnimEnemyReloadModel->setTimePosition(0);   // Reload 
    _AnimEnemyAimModel->setTimePosition(0);   // Aim
    _AnimEnemyAimCrouchModel->setTimePosition(0);   // Aimcrouch  
    _AnimEnemyReloadCrouchModel->setTimePosition(0);   // Reload crouch         
    _AnimEnemyDeadModel->setTimePosition(0);   // dead crouch  
    
    _AnimEnemyRunModel->setEnabled(false);     //run    
    _AnimEnemyWalkModel->setEnabled(false);     //Walk
    _AnimEnemyIdleModel->setEnabled(false);     //Idle
    _AnimEnemyCrouchModel->setEnabled(false);   // Crouch
    _AnimEnemyShootModel->setEnabled(false);     //Idle
    _AnimEnemyShootCrouchModel->setEnabled(false);// Shoot Crouch
    _AnimEnemyReloadModel->setEnabled(false);   // Reload 
    _AnimEnemyAimModel->setEnabled(false);   // Aim
    _AnimEnemyAimCrouchModel->setEnabled(false);   // Aimcrouch  
    _AnimEnemyReloadCrouchModel->setEnabled(false);   // Reload crouch      
    _AnimEnemyDeadModel->setEnabled(false);   // dead crouch  
    
    
    

    
}
/*
    Ogre::Real Terrorist::decideAxeY(){
        Vector3 positionPlayer = player->getPlayerNode()->_getDerivedPosition();
        Vector3 positionTerrorist = _rigidBody->getSceneNode()->_getDerivedPosition();
        Ogre::Real distance = positionTerrorist.distance(positionPlayer);

    }
    
    Ogre::Real Terrorist::decideAxeX(){
            Vector3 positionPlayer = player->getPlayerNode()->_getDerivedPosition();
        Vector3 positionTerrorist = _rigidBody->getSceneNode()->_getDerivedPosition();
        Ogre::Real distance = positionTerrorist.distance(positionPlayer);
std::random_device device;
std::mt19937 generador(device());
std::uniform_real_distribution<> distribucion(.01f, 3.f);

std::cout<< "Numero entre 0.01 y 3.00 : "<< distribucion(generador)<< '\n';

        if(distance>70){
            aux = std::uniform_real_distribution<> distribucion(.01f, 8.f);
        }
        else if(distance>40){
            aux = std::uniform_real_distribution<> distribucion(.01f, 4.f);
        }
        else if(distance>10){
            aux = std::uniform_real_distribution<> distribucion(.01f, 07.f);
        }
        else{
            aux = std::uniform_real_distribution<> distribucion(.01f, 02.f);
        } 
    
        return distribucion(generador);
    }
*/    
    void Terrorist::reduce(Ogre::int32 i){
    if((test-i) <= 0) test = 0; //Si la resta es negativa, le asigno 0
    else test= test - i; //En caso contrario resto.
}
    void Terrorist::parche(){
         float F = 20; 

	Vector3 relPos(_rigidBody->getCenterOfMassPosition());
        Vector3 aux = relPos;
        aux.z-=2;
	Vector3 impulse(0.7,0.7,1.0);
	_rigidBody->applyImpulse(impulse * F, relPos); 
      
    }
    
    bool Terrorist::checkDead(){
        if(_id % 2 == 0)
        {
           if(_AnimEnemyDeadModel->getEnabled() && _AnimEnemyDeadModel->hasEnded()) return true;
           else return false;
        }     
        else {
           if(_AnimEnemyDeadCrouchModel->getEnabled() && _AnimEnemyDeadCrouchModel->hasEnded()) return true;
           else return false;
        }
    }
    
    void Terrorist::desactivePhysical(){
        _rigidBody->getBulletRigidBody()->setCollisionFlags(_rigidBody->getBulletRigidBody()->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);

    }    
        
    


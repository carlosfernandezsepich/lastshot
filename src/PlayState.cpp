#include "PlayState.h"
#include "PauseState.h"
#include "MenuState.h"
#include "Score.h"
#include <string>
#include <iostream> 
#include <stdlib.h>
#include <time.h>  
#include <math.h>
#include <OGRE/Overlay/OgreOverlayElement.h>
#include <cegui-0/CEGUI/MouseCursor.h>


#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"		
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBulletCollisionsRay.h"

#include "IA.h"




using namespace std;
template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

  Ogre::Vector3 _vMoveMouse(0,0,0); //Vector3 from Mouse
  Ogre::Vector3 vt(0,0,0);
void
PlayState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _root->getAutoCreatedWindow()->removeAllViewports();
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  double width = _viewport->getActualWidth();
  double height = _viewport->getActualHeight();
  _camera->setAspectRatio(width / height);
  // Nuevo background colour.
//  _viewport->setBackgroundColour(Ogre::ColourValue(0.05, 0.5, 1.0));
  _sceneMgr->setAmbientLight(Ogre::ColourValue(0.8, 0.8, 0.8));
   //Ogre::OverlaySystem*        mOverlaySystem;
   //mOverlaySystem = OGRE_NEW Ogre::OverlaySystem();
  //_sceneMgr->addRenderQueueListener(mOverlaySystem);
//  _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);;
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_ADDITIVE);
    _sceneMgr->setShadowFarDistance(512);
    _sceneMgr->setShadowColour(ColourValue(0.7, 0.73, 0.75));
    _sceneMgr->setShadowTextureSize(1024);
   // _sceneMgr->setSkyBox(true, "SkyBox", 1100);

  _shoot = false;
  _timeLastShoot = 0.0;
  //-------------
   _numEntities = 0;    // Numero de Shapes instanciadas
  _timeLastObject = 0; // Tiempo desde que se añadio el ultimo objeto

  // Creacion del modulo de debug visual de Bullet ------------------
  _debugDrawer = new OgreBulletCollisions::DebugDrawer();
  _debugDrawer->setDrawWireframe(false);	 
  SceneNode *node = _sceneMgr->getRootSceneNode()->
    createChildSceneNode("debugNode", Vector3::ZERO);
  node->attachObject(static_cast <SimpleRenderable *>(_debugDrawer));

  // Creacion del mundo (definicion de los limites y la gravedad) ---
  AxisAlignedBox worldBounds = AxisAlignedBox (
    Vector3 (-10000, -10000, -10000), 
    Vector3 (10000,  10000,  10000));
  Vector3 gravity = Vector3(0, -9.8, 0);

  _world = new OgreBulletDynamics::DynamicsWorld(_sceneMgr,
 	   worldBounds, gravity);
  _world->setDebugDrawer (_debugDrawer);
  _world->setShowDebugShapes (false);  // Muestra los collision shapes
  
  
  
  //---NEW FUNCTIONS---
  loadResources();

  KeyPressed = OIS::KC_Z;   
  MousePressed = OIS::MB_Button3;
  createCamera();
  setLights();
  p = new Player(_sceneMgr, _camera, _world, _viewport);
 // _shapes.push_back(p->getCollisionShape());   _bodies.push_back(p->getRigidBody());
  loadMap(muestra_contenido_de("media/models/map"));
  createTerrorist(muestra_contenido_de("media/models/spawn"));

   //---------------  
  _exitGame = false;
  // GameManager::getSingletonPtr()->getTrackPtr()->play();     //Play main music
   myWeapon = new Weapon();
   myWeapon->setCapacity(10);
   myWeapon->add_ammo_total(40); //Cambiar a 60
   myWeapon->reload_charger();
   _reload = false;
   _presenIsFinished = false;
   _positionWeapon = false;
   _contador = 0; //Temporal
  
    initializeIA();
  //OVERLAYS
   //createBlood();
   createInfoTimePnl();
   createInfoBulletsPnl();
   createInfoLifePnl();
   createReticle();
   createInfoBullets(); 
   createInfoTime(); 

   _gameStarted = false;
   _soundReproduced = false;
   _timeGame = 120.00;
   _timeSound = 0;
   score = 0;
   _timeForStar = 0.0;
  //createPanelGoGame();
   _gameFinished = false;
   _panelSaving = false;
   _timeFinished = 0.0;
  // lifePlayer = p->getLife();
   num = 0;
   _gameOverTime = 0;
   _gameWinTime = 0;
   _gameOver = false;
   _panelCreated = false;
   _panelCreatedWin = false;
   bloobEnemy = false;
   timeBloodEnemy = 0.0;
     CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();
}
void
PlayState::exit ()
{
    
    //try{
  if(GameManager::getSingletonPtr()->getGameWinPtr()->isPlaying())
      GameManager::getSingletonPtr()->getGameWinPtr()->stop();  
  
  OverlayManager::getSingletonPtr()->destroyAllOverlayElements();
  OverlayManager::getSingletonPtr()->destroyAll();
  
  _root->getAutoCreatedWindow()->removeAllViewports();
  
  terrorists.clear();
 /* _sceneMgr->destroyAllAnimationStates();
  _sceneMgr->destroyAllCameras();
  _sceneMgr->destroyAllLights();
  _sceneMgr->destroyAllParticleSystems(); */
  
  //for(int i = 0; i<terrorists.size(); i++) terrorists.at(i)->exitGame(); //Aviso que es el fin
// Eliminar cuerpos rigidos --------------------------------------
  //delete p; delete terrorists;
  std::deque <OgreBulletDynamics::RigidBody *>::iterator 
     itBody = _bodies.begin();
  while (_bodies.end() != itBody) {   
    delete *itBody;  ++itBody;
  } 
 
  // Eliminar formas de colision -----------------------------------
  std::deque<OgreBulletCollisions::CollisionShape *>::iterator 
    itShape = _shapes.begin();
  while (_shapes.end() != itShape) {   
    delete *itShape; ++itShape;
  } 

  _bodies.clear();  _shapes.clear();

  // Eliminar mundo dinamico y debugDrawer -------------------------
  delete _world->getDebugDrawer();    _world->setDebugDrawer(0);
  delete _world;
//  GameManager::getSingletonPtr()->getTrackPtr()->stop();
  
  _sceneMgr->clearScene();
 //   }catch(Exception e){cout<<e.what()<<endl;}
   CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();

}




void
PlayState::pause()
{
      CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();
}

void
PlayState::resume()
{
      CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();
  // Se restaura el background colour.
  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}



bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{
    //Actualizar variables
    _deltaT = evt.timeSinceLastFrame;
    cout<<"DeltaT es: "<<_deltaT<<endl;
    _timeLastShoot+=_deltaT;
    _timeLastObject -= _deltaT;
    _timeSound += _deltaT;
    
    if(!_exitGame)
    {  
    _world->stepSimulation(_deltaT); // Actualizar simulacion Bullet
    CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(_deltaT); //Actualizo CEGUI
    p->update(_deltaT, mRot, KeyPressed, MousePressed);
    mRot = Ogre::Vector2::ZERO;
    string aux = static_cast<std::ostringstream*>(&(std::ostringstream() << p->getLife()))->str();
    updateInfoLife(aux); 
    aIntelligence->updateWorld(_deltaT);
    string sc = static_cast<std::ostringstream*>(&(std::ostringstream() << score))->str();
    updateInfoScore(sc);
    }

    
    
    
    
    //CONDICIONES
     //Si la vida del player es cero, muestro la imagen de GameOver
     if(p->getLife()>0 && terrorists.size()==0 && !_panelCreatedWin) {
         saveScore(); //Guardo el score
         OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
         Ogre::OverlayElement* textBox = _overlayMgr->getOverlayElement("txtTime");
         textBox->hide();            //Oculto tiempo
         Ogre::OverlayElement* textBox2 = _overlayMgr->getOverlayElement("txtBullets");
         textBox2->hide();           //Oculto balas
         Ogre::OverlayElement* textBox3 = _overlayMgr->getOverlayElement("txtLife");
         textBox3->hide();           //Oculto vida
         Ogre::OverlayElement* textBox4 = _overlayMgr->getOverlayElement("txtScore");
         textBox4->hide();           //Oculto Score 
         Ogre::OverlayElement* textBox5 = _overlayMgr->getOverlayElement("txtBulletsTotal");
         textBox5->hide();           //Oculto balas total 
         createPanelWin(); //Creo el panel
         _panelCreatedWin = true; 
         GameManager::getSingletonPtr()->_voiceMissionCompletedFX->play();
         GameManager::getSingletonPtr()->getGameWinPtr()->play(); //Musica epica
         
         

     }
     if(bloobEnemy) timeBloodEnemy+=_deltaT;
     if(bloobEnemy && timeBloodEnemy>=0.4){ bloobEnemy = false; timeBloodEnemy=0.0; _nodeParticleBlood->detachAllObjects(); _sceneMgr->destroyParticleSystem(_particleBlood);_sceneMgr->destroySceneNode(_nodeParticleBlood);} 
     if(_timeSound > 2.0 && !_soundReproduced) {_soundReproduced = true; GameManager::getSingletonPtr()->_voiceLetsgoFX->play();}
     if((p->getLife()==0 || _timeGame<=0.0)&& !_panelCreated) { createPanelGameOver(); _gameOver = true; _panelCreated = true; GameManager::getSingletonPtr()->_voiceMissionFailedFX->play();}
     if(_panelCreatedWin) _gameWinTime += _deltaT; //Si el juego termino, cuento el tiempo.
     if(_gameOver == true) _gameOverTime+=_deltaT; //Si el juego ha finalizado, cuento el tiempo
     if(_gameOver == true && _gameOverTime>6.0) changeState(MenuState::getSingletonPtr()); //Si han pasado 6 seg, salgo del juego   
     if(_panelCreatedWin && _gameWinTime>6.0) changeState(MenuState::getSingletonPtr()); //Si han pasado 6 seg, salgo del juego
     if((!_gameOver && myWeapon->is_empty_total() && myWeapon->is_empty_charger())) { _gameOver = true; GameManager::getSingletonPtr()->_voiceMissionFailedFX->play(); createPanelGameOver(); _panelCreated = true;}//Si se queda sin balas, muestro imagen de GameOver 
     if(_timeGame <= 10.0 && !_gameOver && !_panelCreatedWin) {
            OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
            Ogre::OverlayElement* textBox = _overlayMgr->getOverlayElement("txtTime");
            textBox->setColour(ColourValue(0.8, 0, 0));
     }
     if(!_gameOver && !_panelCreatedWin)
        {
            if(_timeGame>0.0)
             {
                if(_timeGame - _deltaT <0.0) _timeGame = 0.0;
                else _timeGame-=_deltaT;
                float rounded_down = floorf(_timeGame * 100) / 100;
                string aux = static_cast<std::ostringstream*>(&(std::ostringstream() <<rounded_down))->str();
                updateInfoTime(aux);
             }
            else{
                _timeGame = 0.0;
                updateInfoTime("00:00");
            }
        }
     if(_reload && !_gameOver && !_panelCreatedWin)  //Recargar
        {
            if(myWeapon->get_ammo_charger() < myWeapon->getCapacity())
            {
                 p->reload();    
                 myWeapon->reload_charger();
                    if(myWeapon->get_ammo_charger()<10)
                    {
                        string aux = "0";
                        aux+= static_cast<std::ostringstream*>(&(std::ostringstream() <<myWeapon->get_ammo_charger()))->str();
                        updateInfoBullets(aux, Ogre::StringConverter::toString(myWeapon->get_ammo_total())); //Overlay Bullets
                    }
                    else
                    {
                        updateInfoBullets(Ogre::StringConverter::toString(myWeapon->get_ammo_charger()), 
                        Ogre::StringConverter::toString(myWeapon->get_ammo_total())); //Overlay Bullets
                    }
                 _reload = false;
       
            }
        }
    
    if(_shoot == true && _timeLastShoot>0.5 && !_panelCreatedWin &&!_gameOver &&(p->checkShoot()))
   {
       _timeLastShoot = 0.0;  
       if(myWeapon->get_ammo_charger()>0){
           p->shot();
           myWeapon->shoot();
       if(myWeapon->get_ammo_charger()<10){
           string aux = "0";
           aux+= static_cast<std::ostringstream*>(&(std::ostringstream() <<myWeapon->get_ammo_charger()))->str();
           updateInfoBullets(aux,Ogre::StringConverter::toString(myWeapon->get_ammo_total())); //Overlay Bullets
       }
       else{
           updateInfoBullets(Ogre::StringConverter::toString(myWeapon->get_ammo_charger()),
            Ogre::StringConverter::toString(myWeapon->get_ammo_total())); //Overlay Bullets
       }
        Ogre::Vector3 position = OgreBulletCollisions::convert(p->getRigidBody()->getBulletRigidBody()->getCenterOfMassPosition());   
        Ogre::Ray rShoot(position, OgreBulletCollisions::convert(p->getRigidBody()->getBulletRigidBody()->getOrientation()) * Ogre::Vector3::UNIT_Z);
        OgreBulletCollisions::CollisionClosestRayResultCallback cQueryShoot = CollisionClosestRayResultCallback (rShoot, _world, 100);
        _world->launchRay(cQueryShoot);
        string claveAux = "rigidBodyEnemy";
            if (cQueryShoot.doesCollide()) 
            { //Si el rayo detecta una colision
                if(strncmp(cQueryShoot.getCollidedObject()->getName().c_str(), claveAux.c_str(), 14) == 0) //Pregunto si aparentemente le di a un enemigo
                {
                    for(int i = 0; i<terrorists.size(); i++){
                        if(cQueryShoot.getCollidedObject()->getName().c_str() == terrorists.at(i)->getRigidBody()->getName().c_str()) //Busco exactamente cual es el enemigo
                        {
                            terrorists.at(i)->reduce(p->decideHurt(rShoot.getDirection().y));
                            score+=decideScore(rShoot.getDirection().y);
                            createBloodParticle(terrorists.at(i)->getRigidBody()->getCenterOfMassPosition());
                            //simulateBlood(position);
                        //    terrorists.at(i)->ChangeState('m'); //Cambio su estado a Muerto
                        }
                    }
                    //El player le ha dado     
                    cout<<"Le diste a: "<<cQueryShoot.getCollidedObject()->getName()<<endl;
                } 
                        
            }
 
       }else{
       GameManager::getSingletonPtr()->_emptyEffect->play();     //Effects no have bullets
       }     
   }
 
    return true;
}




bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
  _deltaT = evt.timeSinceLastFrame;
  _world->stepSimulation(_deltaT); // Actualizar simulacion Bullet
  CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(_deltaT); //Actualizo CEGUI
  p->update(_deltaT, mRot, KeyPressed, MousePressed); 
  aIntelligence->updateWorld(_deltaT);
  if (_exitGame)
    return false;
  
  return true;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
      
  KeyPressed = e.key;
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(e.key));
  CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(e.text);  
  //---------PAUSE-----------  
  if (e.key == OIS::KC_P) {
    pushState(PauseState::getSingletonPtr());
  }
    if (e.key == OIS::KC_ESCAPE) {
    pushState(PauseState::getSingletonPtr());
    //_exitGame = true;
  }

  //---------Recharge----------- 
  if (e.key == OIS::KC_R) {
      _reload = true;
      
  }
}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
   
   CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(static_cast<CEGUI::Key::Scan>(e.key));   
   KeyPressed = OIS::KC_Z;
}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
    
    if(_gameFinished == false){
        
            mRot.x = e.state.X.rel;
            mRot.y = e.state.Y.rel;

    }else{
      CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);} 
    

}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(id == OIS::MB_Left)
    {
       MousePressed = OIS::MB_Left;  
       _shoot = true;
    }
    
    
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
     if(id == OIS::MB_Left)
    {
       MousePressed = OIS::MB_Button3;  
       _shoot = false;
    }
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton PlayState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

PlayState*
PlayState::getSingletonPtr ()
{
return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

void PlayState::loadResources(){
     Ogre::ConfigFile cf;
  cf.load("resources.cfg");

  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
  Ogre::String sectionstr, typestr, datastr;
  while (sI.hasMoreElements()) {
    sectionstr = sI.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typestr = i->first;    datastr = i->second;
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation
            (datastr, typestr, sectionstr);
    }
  }
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

std::vector<string> PlayState::muestra_contenido_de(const std::string &a_carpeta)
{
    boost::filesystem::path p(a_carpeta);
    std::vector<string> files;

    if (boost::filesystem::exists(p) && boost::filesystem::is_directory(p))
    {
    //    std::cout << p << "contiene:\n";

        for (auto &x : boost::filesystem::directory_iterator(p)){
    //          cout << "    " << x.path() << '\n';
              if(x.path().extension() == ".mesh")
              files.push_back(x.path().filename().string());
        }
    }
    return files;
}


void PlayState::loadMap(std::vector<string> pMeshes){
  // Creamos forma de colision para el plano ----------------------- 
  OgreBulletCollisions::CollisionShape *Shape;
  Shape = new OgreBulletCollisions::StaticPlaneCollisionShape
    (Ogre::Vector3(0,1,0), 0);   // Vector normal y distancia
  OgreBulletDynamics::RigidBody *rigidBodyPlane = new 
    OgreBulletDynamics::RigidBody("rigidBodyPlane", _world);

  // Creamos la forma estatica (forma, Restitucion, Friccion) ------
  rigidBodyPlane->setStaticShape(Shape, 0.1, 0.0); 
  
  // Anadimos los objetos Shape y RigidBody ------------------------
  _shapes.push_back(Shape);      _bodies.push_back(rigidBodyPlane);
  
  
  Ogre::SceneNode *auxNode;
  Ogre::Entity *auxEnt;
  Ogre::Vector3 size, position;
  Ogre::Quaternion rotation;
  OgreBulletDynamics::RigidBody *rigidBodyAux;
  OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter = NULL; 
  OgreBulletCollisions::TriangleMeshCollisionShape *bodyShape = NULL;
  string claveAux = "collision";
  string claveAuxFloor = "Mesh.879";
  string claveAuxVereda = "sidewalk";
  string claveAuxRoad = "road";
  string claveAuxBoule = "boulevard";
  string claveAuxSky = "skyDome";
  std::vector<Ogre::Vector3> positionsEnemies;
  for(int i = 0; i<pMeshes.size(); i++){
      if(strncmp(pMeshes.at(i).c_str(), claveAux.c_str(), 8) == 0){
      string nameNode = "SG_Map";
      nameNode+=static_cast<std::ostringstream*>(&(std::ostringstream() << i))->str();
      auxNode = _sceneMgr->createSceneNode(nameNode);
      auxEnt = _sceneMgr->createEntity(pMeshes.at(i));
      auxNode->attachObject(auxEnt);
      auxNode->setVisible(false);
      _sceneMgr->getRootSceneNode()->addChild(auxNode);
      auxNode->translate(0,0.1,0);
      size = Vector3::ZERO;	
      position = auxNode->_getDerivedPosition();
      rotation = auxNode->_getDerivedOrientation();
      rigidBodyAux = NULL;
      trimeshConverter = new OgreBulletCollisions::StaticMeshToShapeConverter(auxEnt);
      bodyShape = trimeshConverter->createTrimesh();
      rigidBodyAux = new OgreBulletDynamics::RigidBody("rigidBody" + nameNode, _world);
      rigidBodyAux->setShape(auxNode, bodyShape,
		     0 /* Restitucion */, 0.8 /* Friccion */,
		     0 /* Masa */, position /* Posicion inicial */,
		     rotation /* Orientacion */);
      rigidBodyAux->showDebugShape(false);
      //rigidBodyAux->getBulletRigidBody()->setCollisionFlags(rigidBodyAux->getBulletRigidBody()->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE); 
      rigidBodyAux->getBulletObject()->setUserPointer((void *) auxNode);
      _shapes.push_back(bodyShape);   _bodies.push_back(rigidBodyAux);
  
      }
      else if(strncmp(pMeshes.at(i).c_str(), claveAuxFloor.c_str(), 8) == 0 ||
              strncmp(pMeshes.at(i).c_str(), claveAuxVereda.c_str(), 8) == 0 ||
              strncmp(pMeshes.at(i).c_str(), claveAuxRoad.c_str(), 4) == 0 ||
              strncmp(pMeshes.at(i).c_str(), claveAuxBoule.c_str(), 9) == 0 ||
              strncmp(pMeshes.at(i).c_str(), claveAuxSky.c_str(), 7) == 0)
      { //Cargo el suelo
          cout<<"Entra OK-------"<<endl;
      string nameNode = "SG_Map";
      nameNode+=static_cast<std::ostringstream*>(&(std::ostringstream() << i))->str();
      auxNode = _sceneMgr->createSceneNode(nameNode);
      auxEnt = _sceneMgr->createEntity(pMeshes.at(i));
      auxEnt->setCastShadows(false);
      auxNode->attachObject(auxEnt);
      _sceneMgr->getRootSceneNode()->addChild(auxNode);
      }
      
      else{
      string nameNode = "SG_Map";
      nameNode+=static_cast<std::ostringstream*>(&(std::ostringstream() << i))->str();
      auxNode = _sceneMgr->createSceneNode(nameNode);
      auxEnt = _sceneMgr->createEntity(pMeshes.at(i));
      auxEnt->setCastShadows(true);
      auxNode->attachObject(auxEnt);
      _sceneMgr->getRootSceneNode()->addChild(auxNode);
      }
  }  
  
  
}
 
void PlayState::updateAnimations(){

    for(int i=0; i<terrorists.size(); i++)
   {
       terrorists.at(i)->updateAnimations(_deltaT);
   }
}

void PlayState::initializeIA(){
    aIntelligence = new IA(&terrorists, p->getRigidBody(), _world, myWeapon);
}

void PlayState::createTerrorist(std::vector<string> pMeshes){

    SceneNode *_nodeEnemy; Entity *_entEnemy;
    SceneNode *auxNode; Entity *auxEnt;
    SceneNode *_nodeArm; Entity *_entArm;
    OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter = NULL; 
    OgreBulletCollisions::CollisionShape *bodyShape = NULL;
    OgreBulletDynamics::RigidBody *rigidBody = NULL;
    std::vector<Ogre::Vector3> enemiesPositions;
    enemiesPositions.push_back(Ogre::Vector3(30.85373, 0.0, -11.1977)); //
    enemiesPositions.push_back(Ogre::Vector3(6.0, 0.0, -26.0)); //33.85693, 0.0, -2.34414
    enemiesPositions.push_back(Ogre::Vector3(-1.0, 0.0, -34.0)); //37.25169, 0.0, -2.34414
//    enemiesPositions.push_back(Ogre::Vector3(30.85373, 5.0, 2)); //Ogre::Vector3(36.5871, 0.0, -6.5174
    enemiesPositions.push_back(Ogre::Vector3(0.0, 0.0, 0.0));
///   enemiesPositions.push_back(Ogre::Vector3(38.2252, 0.0, -12.13376));
   // enemiesPositions.push_back(Ogre::Vector3(34.20795, 0.0, -13.22583));
   // enemiesPositions.push_back(Ogre::Vector3(30.85373, 0.0, -11.1977)); 
    string num;
    Entity *entTerrorist = _sceneMgr->createEntity("ShapeTerrorist","ShapeTerrorist.mesh");
    for(int i=0; i<enemiesPositions.size(); i++){

        if(i%2==0)
        {
        num = static_cast<std::ostringstream*>(&(std::ostringstream() << i))->str();
        _entEnemy = _sceneMgr->createEntity("EnemyEnt"+num,"baseBot.mesh");
        _nodeEnemy = _sceneMgr->createSceneNode("NodeEnemy" + num);
        _nodeEnemy->attachObject(_entEnemy);
        _sceneMgr->getRootSceneNode()->addChild(_nodeEnemy);     
        _nodeEnemy->setPosition(enemiesPositions.at(i));
        _entEnemy->setCastShadows(true);
         if(enemiesPositions.at(i).x >= 30)
            { 
                _nodeEnemy->yaw(Degree(-180));
            }
        }else{
                num = static_cast<std::ostringstream*>(&(std::ostringstream() << i))->str();
        _entEnemy = _sceneMgr->createEntity("EnemyEnt"+num,"baseBot2.mesh");
        _nodeEnemy = _sceneMgr->createSceneNode("NodeEnemy" + num);
        _nodeEnemy->attachObject(_entEnemy);
        _sceneMgr->getRootSceneNode()->addChild(_nodeEnemy);     
        _nodeEnemy->setPosition(enemiesPositions.at(i));
        _entEnemy->setCastShadows(true);
        if(enemiesPositions.at(i).x >= 30){ 
       _nodeEnemy->yaw(Degree(-180));
        }
        }
       
         
        Vector3 size = Vector3::ZERO;	
        Vector3 position = _nodeEnemy->_getDerivedPosition();
        Quaternion rotation = _nodeEnemy->_getDerivedOrientation();
        // Obtenemos la bounding box de la entidad creada... ------------

        trimeshConverter = new OgreBulletCollisions::StaticMeshToShapeConverter(entTerrorist);
        bodyShape = trimeshConverter->createConvex();
        delete trimeshConverter;
  
        rigidBody = new OgreBulletDynamics::RigidBody("rigidBodyEnemy" + StringConverter::toString(i), _world);
        rigidBody->setShape(_nodeEnemy, bodyShape,
		     0 /* Restitucion */, 0 /* Friccion */,
		     80 /* Masa */, position /* Posicion inicial */,
		     rotation /* Orientacion */);
        rigidBody->getBulletObject()->setUserPointer((void *) _nodeEnemy);
        rigidBody->showDebugShape(false);
        // Anadimos los objetos a las deques
    //    rigidBody->setOrientation(rigidBody->getSceneNode()->getOrientation() * Ogre::Quaternion(Ogre::Degree(-45) , Ogre::Vector3::UNIT_Y));
        _shapes.push_back(bodyShape);   _bodies.push_back(rigidBody);
                   
        unTerrorista = new Terrorist(i+1, _entEnemy, rigidBody, _world, p);
        unTerrorista->Inicializar();    
        terrorists.push_back(unTerrorista);


    }
}


void PlayState::createCamera(){

  _camera->setPosition(0.3, 1.95, -1);
  _camera->setDirection(-0.0221544, -0.0517451, 0.998396);
  _camera->setOrientation(Ogre::Quaternion(-0.0110891, 0.000287076, -0.999599, -0.0258798));
  _camera->setNearClipDistance(0.1);
  _camera->setFarClipDistance(600.000); 
}

void PlayState::setLights(){
   
/*Light* light = _sceneMgr->createLight("Sun");
light->setType(Light::LT_DIRECTIONAL);
light->setDirection(Vector3(0, -1, -0.5));*/
/*
Light* light2 = _sceneMgr->createLight("Light2");
light2->setType(Light::LT_POINT);
light2->setPosition(0, 20, 0);
light2->setSpecularColour(0.9, 0.9, 0.9);
light2->setDiffuseColour(0.9, 0.9, 0.9);
SceneNode *s = _sceneMgr->getRootSceneNode()->createChildSceneNode("s");
s->attachObject(light2);
s->yaw(Degree(5));
s->roll(Degree(3));
 * 
*/
Light* light2 = _sceneMgr->createLight("Light10");
light2->setPosition(0,20,0);
light2->setType(Light::LT_SPOTLIGHT);
light2->setDirection(Vector3(1,-1,0));
light2->setSpotlightInnerAngle(Degree(-360.0f));
light2->setSpotlightOuterAngle(Degree(360.0f));
light2->setSpotlightFalloff(10.0f);
light2->setCastShadows(true);



 /*   
  _light2 = _sceneMgr->createLight("Light02");
  _light2->setPosition(0,30,0);
  _light2->setType(Ogre::Light::LT_DIRECTIONAL);
  _light2->setDirection(Ogre::Vector3(0,0,0));
  */
/* 
  _light3 = _sceneMgr->createLight("Light03");
  _light3->setPosition(0,10,0);
  _light3->setDiffuseColour(0, 0, 0);
  _light3->setType(Ogre::Light::LT_SPOTLIGHT);
  _light3->setDirection(Ogre::Vector3(0,0,0));
  _light3->setSpotlightInnerAngle(Ogre::Degree(0.0f));
  _light3->setSpotlightOuterAngle(Ogre::Degree(90.0f));
  _light3->setSpotlightFalloff(1.0f);
  _light3->setCastShadows(true);

  /*  _light = _sceneMgr->createLight("Light01");
  _light->setPosition(0,10,0);
  _light->setType(Ogre::Light::LT_DIRECTIONAL);
  _light->setDirection(Ogre::Vector3(10,10,10));
  _light->setCastShadows(true); */
  //_nodeGun->attachObject(_light);

}

//NUEVOS OVERLYAS PANELS
void PlayState::createInfoTimePnl(){

    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Ogre::Overlay *_overlay = _overlayMgr->create("overlayInfoTimePanel");
    OverlayContainer* _panel = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerInfoTime"));
    _panel->setMetricsMode(Ogre::GMM_PIXELS);
    _panel->setPosition(width*0.35, 0);
    _panel->setDimensions(width*0.30, height*0.125);
    _panel->setMaterialName("topHUD"); // Optional background material
 
    _overlay->add2D(_panel);
 
    _overlay->show();
}

void PlayState::createInfoBulletsPnl(){

    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Ogre::Overlay *_overlay = _overlayMgr->create("overlayInfoBulletsPanel");
    OverlayContainer* _panel = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerInfoBullets"));
    _panel->setMetricsMode(Ogre::GMM_PIXELS);
    _panel->setPosition(0, height*0.85);
    _panel->setDimensions(width*0.25, height*0.15);
    _panel->setMaterialName("leftHUD"); // Optional background material

    
    _overlay->add2D(_panel);
 
    _overlay->show();
}

void PlayState::createInfoLifePnl(){

    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Ogre::Overlay *_overlay = _overlayMgr->create("overlayInfoLifePanel");
    OverlayContainer* _panel = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerInfoLife"));
    _panel->setMetricsMode(Ogre::GMM_PIXELS);
    _panel->setPosition(width*0.75, height*0.85);
    _panel->setDimensions(width*0.25, height*0.15);
    _panel->setMaterialName("rightHUD"); // Optional background material
 
    _overlay->add2D(_panel);
 
    _overlay->show();
}
//-----------------------
void PlayState::createOverlay() {
//Ogre::Overlay* pMyOverlayLogo = (Overlay*) OverlayManager::getSingleton().getByName("Info");
  //pMyOverlayLogo->show();
  OverlayManager& overlayManager = OverlayManager::getSingleton();
 
// Create a panel
OverlayContainer* panel = static_cast<OverlayContainer*>(
    overlayManager.createOverlayElement("Panel", "PanelName"));
panel->setMetricsMode(Ogre::GMM_PIXELS);
panel->setPosition(200, 700);
panel->setDimensions(150, 100);
panel->setMaterialName("panelInfoM"); // Optional background material
 
 
// Create an overlay, and add the panel
Overlay* overlay = overlayManager.create("OverlayName");
overlay->add2D(panel);
//overlay->add2D(textArea);
 
// Add the text area to the panel
//panel->addChild(textArea);
 
// Show the overlay
overlay->show();
  
}

void PlayState::createReticle(){
    
    OverlayManager& overlayManager = OverlayManager::getSingleton();
 
// Create a panel
OverlayContainer* panel = static_cast<OverlayContainer*>(
    overlayManager.createOverlayElement("Panel", "PanelReticle"));
panel->setMetricsMode(Ogre::GMM_PIXELS);
double width = _viewport->getActualWidth();
double height = _viewport->getActualHeight();
panel->setDimensions(100, 25);
panel->setPosition((width-panel->getWidth())/2, (height-panel->getHeight())/2);
panel->setMaterialName("materialReticle"); // Optional background material

// Create an overlay, and add the panel
Overlay* overlay = overlayManager.create("OverlayReticle");
overlay->add2D(panel); 
// Show the overlay
overlay->show();
}

void PlayState::createInfoBullets(){

    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Ogre::Overlay *_overlay = _overlayMgr->create("overlayInfoBullets");
    OverlayContainer* _panel = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerBullets"));
    _panel->setDimensions(100, 100);
    _panel->setPosition(0, 0);
 
    _overlay->add2D(_panel);
 
    _overlay->show();
    
    	Ogre::FontManager &fontMgr = Ogre::FontManager::getSingleton();
	Ogre::ResourcePtr font = fontMgr.create("MyFont","General");
	font->setParameter("type","truetype");
	font->setParameter("source","HelveticaBold.ttf");
	font->setParameter("size","46");
	font->setParameter("resolution","96");
        font->load();
        
	Ogre::ResourcePtr fontNormal = fontMgr.create("MyFontNormal","General");
	fontNormal->setParameter("type","truetype");
	fontNormal->setParameter("source","HelveticaBold.ttf");
	fontNormal->setParameter("size","46");
	fontNormal->setParameter("resolution","96");
        fontNormal->load();
        
	Ogre::ResourcePtr fontSmall = fontMgr.create("MyFontSmall","General");
	fontSmall->setParameter("type","truetype");
	fontSmall->setParameter("source","HelveticaBold.ttf");
	fontSmall->setParameter("size","16");
	fontSmall->setParameter("resolution","96");
        fontSmall->load();
    
    
}

void PlayState::createInfoTime(){

    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    string l1 = "";
    string l2 = "";
    string l3 = "";
    string l4 = "";
    string l5 = "";
    if(width<500)
    {
        l1 = "19";
        l2 = "8";
        l3 = "13";
        l4 = "20";
        l5 = "16";
    }
    else if (width<900){
        l1 = "23";
        l2 = "11";
        l3 = "19";
        l4 = "43";
        l5 = "18";
    }
    else{
        l1 = "45";
        l2 = "20"; 
        l3 = "36";
        l4 = "63";
        l5 = "43";
    }
    
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Ogre::Overlay *_overlay = _overlayMgr->create("overlayInfoTime");
    OverlayContainer* _panel = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerTime"));
    _panel->setMetricsMode(Ogre::GMM_PIXELS);
    _panel->setPosition(width*0.35, 1);
    _panel->setDimensions(width*0.35, height*0.2);
   // _panel->setMaterialName("topHUD"); // Optional background material
 
    _overlay->add2D(_panel);
 
    _overlay->show();
    
    //--------------------TEXT TIME---------------------------------------------
    Ogre::OverlayElement *textBox = _overlayMgr->createOverlayElement("TextArea", "txtTime");
    textBox->setDimensions(100, 100);
    textBox->setMetricsMode(Ogre::GMM_PIXELS);
    textBox->setPosition(width*0.067 , height*0.025);
    textBox->setParameter("font_name", "MyFont");
    textBox->setParameter("char_height", l4);
    textBox->setColour(Ogre::ColourValue::White);
 
    textBox->setCaption("120:00");
 
    _panel->addChild(textBox);
    
    //-----------------------TEXT BULLETS---------------------------------------
    Ogre::OverlayElement *textBoxBullets = _overlayMgr->createOverlayElement("TextArea", "txtBullets");
    textBoxBullets->setDimensions(50, 50);
    textBoxBullets->setMetricsMode(Ogre::GMM_PIXELS);
    textBoxBullets->setPosition(width*-0.20, height*0.93);
    textBoxBullets->setParameter("font_name", "MyFont");
    textBoxBullets->setParameter("char_height", l4);
    textBoxBullets->setParameter("char_width", "10");
    textBoxBullets->setColour(Ogre::ColourValue::White);
 
    textBoxBullets->setCaption("10");
 
    _panel->addChild(textBoxBullets);
    
    Ogre::OverlayElement *textBoxBulletsTotal = _overlayMgr->createOverlayElement("TextArea", "txtBulletsTotal");
    textBoxBulletsTotal->setDimensions(100, 100);
    textBoxBulletsTotal->setMetricsMode(Ogre::GMM_PIXELS);
    textBoxBulletsTotal->setPosition(width*-0.22, height*0.94);
    textBoxBulletsTotal->setParameter("font_name", "MyFontSmall");
    textBoxBulletsTotal->setParameter("char_height", l2);
    textBoxBulletsTotal->setColour(Ogre::ColourValue::White);
 
    textBoxBulletsTotal->setCaption("30");
 
    _panel->addChild(textBoxBulletsTotal);
    
    //-----------------------------TEXT SCORE-----------------------------------
    Ogre::OverlayElement *textBoxScore = _overlayMgr->createOverlayElement("TextArea", "txtScore");
    textBoxScore->setDimensions(100, 100);
    textBoxScore->setMetricsMode(Ogre::GMM_PIXELS);
    textBoxScore->setPosition(width*0.205, height*0.025);
    textBoxScore->setParameter("font_name", "MyFont");
    textBoxScore->setParameter("char_height", l5 );
    textBoxScore->setColour(Ogre::ColourValue::White);
 
    textBoxScore->setCaption("000");
 
    _panel->addChild(textBoxScore);
    
        //-----------------------------TEXT LIFE-----------------------------------
    Ogre::OverlayElement *textBoxLife = _overlayMgr->createOverlayElement("TextArea", "txtLife");
    textBoxLife->setDimensions(100, 100);
    textBoxLife->setMetricsMode(Ogre::GMM_PIXELS);
    textBoxLife->setPosition(width*0.48, height*0.93);
    textBoxLife->setParameter("font_name", "MyFont");
    textBoxLife->setParameter("char_height", l5);
    textBoxLife->setColour(Ogre::ColourValue::White);
 
    textBoxLife->setCaption("100");
 
    _panel->addChild(textBoxLife);
}

void PlayState::updateInfoBullets(Ogre::String pText, Ogre::String pText2){
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Ogre::OverlayElement* textBox = _overlayMgr->getOverlayElement("txtBullets");
    textBox->setCaption(pText);
    
    Ogre::OverlayElement* textBox2 = _overlayMgr->getOverlayElement("txtBulletsTotal");
    textBox2->setCaption(pText2);
    
}

void PlayState::updateInfoTime(Ogre::String pText){
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Ogre::OverlayElement* textBox = _overlayMgr->getOverlayElement("txtTime");
    textBox->setCaption(pText);
}

void PlayState::updateInfoLife(Ogre::String life){
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Ogre::OverlayElement* textBox = _overlayMgr->getOverlayElement("txtLife");
    textBox->setCaption(life);
}

void PlayState::updateInfoScore(Ogre::String pText){
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Ogre::OverlayElement* textBox = _overlayMgr->getOverlayElement("txtScore");
    textBox->setCaption(pText);
}

  void PlayState::createPanelGoGame(){
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
  
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    _overlayGoGame = _overlayMgr->create("overlayPanelGoGame");
    _panelGoGame = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerGoGame"));
    _panelGoGame->setMetricsMode(Ogre::GMM_PIXELS);
    _panelGoGame->setPosition(width*0.45, height*0.45);
    _panelGoGame->setDimensions(width*0.15, height*0.20);
    _panelGoGame->setMaterialName("panelGoGame3"); // Optional background material
    _overlayGoGame->add2D(_panelGoGame);
 
    _overlayGoGame->show();
  }
  void PlayState::updatePanelGoGame(int pNumber){

      _panelGoGame->setEnabled(true);
      switch(pNumber){
          case 0: _overlayGoGame->clear();
              break;
          case 1: _panelGoGame->setMaterialName("panelGoGamego");
              break;
          case 2: _panelGoGame->setMaterialName("panelGoGame1");
              break;
          case 3: _panelGoGame->setMaterialName("panelGoGame2");
              break;   
      }
  }
 
   void PlayState::createPanelSaveScore(){
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
  
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Overlay *_overlaySaveScore = _overlayMgr->create("overlayPanelSaveScore");
    OverlayContainer* _panelSaveScore = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerSaveScore"));
    _panelSaveScore->setMetricsMode(Ogre::GMM_PIXELS);
    _panelSaveScore->setPosition(width*0.45, height*0.45);
    _panelSaveScore->setDimensions(width*0.20, height*0.25);
    _panelSaveScore->setMaterialName("panelSaveScore"); // Optional background material
    _overlaySaveScore->add2D(_panelSaveScore);
 
    _overlaySaveScore->show();
  }
   
   void PlayState::createPanelGameOver(){
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
  
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Overlay *_overlayGameOver = _overlayMgr->create("overlayPanelGameOver");
    OverlayContainer* _panelGameOver = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerGameOver"));
    _panelGameOver->setMetricsMode(Ogre::GMM_PIXELS);
//    _panelGameOver->setPosition(width*0.25, height*0.45);
    _panelGameOver->setDimensions(width, height);
    _panelGameOver->setMaterialName("panelGameOver"); // Optional background material
    _overlayGameOver->add2D(_panelGameOver);
 
    _overlayGameOver->show();
  }
   
    void PlayState::createPanelWin(){
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
  
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Overlay *_overlayGameOver = _overlayMgr->create("overlayPanelGameWin");
    OverlayContainer* _panelGameOver = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerGameWin"));
    _panelGameOver->setMetricsMode(Ogre::GMM_PIXELS);
//    _panelGameOver->setPosition(width*0.25, height*0.45);
    _panelGameOver->setDimensions(width, height);
    _panelGameOver->setMaterialName("panelGameWin"); // Optional background material
    _overlayGameOver->add2D(_panelGameOver);
 
    _overlayGameOver->show();
  } 
   
  void PlayState::deletePanelSaveScore(){
      OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
      Overlay *_overlaySaveScore = _overlayMgr->getByName("overlayPanelSaveScore");
      _overlaySaveScore->clear();
  }
 
  void PlayState::saveScore(){ 
    std::stringstream channel;
    string name = "-- ";
    name+= GameManager::getSingletonPtr()->getNamePlayer();
    for(int i = 0; i < name.length(); i++)
        {
            channel << name[i]; //Convert to format
        }      
    ScoreManager::write_file(channel.str(), score); //Write in file txt name and score 
   // CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->destroy(); //delete window
  //  deletePanelSaveScore();
    //  pause();
   // changeState(MenuState::getSingletonPtr());
} 
   



int PlayState::decideScore(Ogre::Real y){
    cout<<"Y del disparo es:"<<y<<endl;
    if(y < 0.003) return 10;
    else if (y < 0.005) return 16;
    else return 20;
}
void PlayState::createBloodParticle(Vector3 position){

  bloobEnemy = true;  
  position.y+=0.5;  
  //string aux = static_cast<std::ostringstream*>(&(std::ostringstream() << myWeapon->get_ammo_charger()))->str(); 
  //string aux2 = static_cast<std::ostringstream*>(&(std::ostringstream() << myWeapon->get_ammo_total()))->str(); 
  _particleBlood = _sceneMgr->createParticleSystem("particleBlood","Supernova");
  _nodeParticleBlood = _sceneMgr->getRootSceneNode()->createChildSceneNode("NodeparticleBlood"); 
  _particleBlood->setDefaultDimensions(0.2,0.2);
  //_particleBlood->setRenderingDistance(1.0);
  _particleBlood->setSpeedFactor(0.3);
  _nodeParticleBlood->attachObject(_particleBlood);
  _nodeParticleBlood->setPosition(position);
  //_nodeParticleBlood->setScale(0.1,0.1,0.1);
}

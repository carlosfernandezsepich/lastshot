
#include "Player.h"
#include "GameManager.h"

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"		
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBulletCollisionsRay.h"

#include <OGRE/Overlay/OgreOverlayElement.h>




Player::Player(SceneManager *sceneMgr, Camera *myCam, OgreBulletDynamics::DynamicsWorld *pWorld, Ogre::Viewport *pViewport) {
 
     test = Ogre::int32(100);
     camera = myCam;
     viewport = pViewport;
     nodeSceneMgr = sceneMgr;
     world = pWorld;
     nodeParche = nodeSceneMgr->getRootSceneNode()->createChildSceneNode("NodeParche");
     nodeParche->setPosition(Vector3(-41.89355, 0.5, 41.81267));
     nodeParche->yaw(Degree(90));
     nodePlayer = nodeParche->createChildSceneNode("NodePlayer");
     nodeCamera = nodePlayer->createChildSceneNode("NodeCamera"); 
     nodeCamera->attachObject(myCam);
     nodeArm = nodePlayer->createChildSceneNode("NodeArm"); 
     nodeArm->translate(Vector3(0,-2.05,0), Node::TS_PARENT);
     entM9 = nodeSceneMgr->createEntity("M9Ent","Arma.mesh");
     entM92 = nodeSceneMgr->createEntity("M92Ent", "Arma2.mesh");
     entM9Cargador = nodeSceneMgr->createEntity("M9CargadorEnt", "Cargador.mesh");
     entM9Gatillo = nodeSceneMgr->createEntity("M9GatilloEnt", "Gatillo.mesh");
     entBody = nodeSceneMgr->createEntity("BodyEnt", "Cuerpo.mesh");

    
     nodeArm->attachObject(entM9); nodeArm->attachObject(entM92);
     nodeArm->attachObject(entM9Cargador); nodeArm->attachObject(entM9Gatillo);
     nodeArm->attachObject(entBody);
     nodeArm->yaw(Ogre::Degree(180), Node::TS_LOCAL);
     timeBlood = 0;
     timeDead = 0;
   //  life = 100;
     overlayHerido(); _panelP->hide(); //Creo el overlay y lo oculto
     
     Vector3 size = Vector3::ZERO;	// size of the box
  
     Vector3 position(nodeParche->_getDerivedPosition());  
     Ogre::Quaternion rotation = nodeParche->_getDerivedOrientation();
     // Obtenemos la bounding box de la entidad creada... ------------
    // AxisAlignedBox boundingB = nodeSceneMgr->getEntity("BoxPlayer")->getBoundingBox();
    OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter = NULL; 
    OgreBulletCollisions::CollisionShape *bodyShape = NULL;
    Entity *entP = nodeSceneMgr->createEntity("ShapePlayer","ShapeTerrorist.mesh");
    trimeshConverter = new OgreBulletCollisions::StaticMeshToShapeConverter(entP);
    bodyShape = trimeshConverter->createConvex();
    delete trimeshConverter;
     // and the Bullet rigid body
     rigidBody = new OgreBulletDynamics::RigidBody("rigidBodyPlayer", world);

     rigidBody->setShape(nodeParche, bodyShape,
		     0 /* Restitucion */, 0 /* Friccion */,
		     60 /* Masa */, position /* Posicion inicial */,
		     rotation /* Orientacion */);
    // rigidBody->setKinematicObject(true);
    // rigidBody->showDebugShape(false);
    rigidBody->getBulletObject()->setUserPointer((void *) nodeParche);
    
     loadAnimations();
     
        //PARCHE para que el arma aparezca bien ubicada
     
       _AnimM9Shoot->setTimePosition(0);
       _AnimM9_2Shoot->setTimePosition(0);
       _AnimM9GatilloShoot->setTimePosition(0);
       _AnimM9CargadorShoot->setTimePosition(0);
       _AnimBodyM9Shoot->setTimePosition(0);
       _AnimM9Shoot->setLoop(false);
       _AnimM9_2Shoot->setLoop(false);
       _AnimM9GatilloShoot->setLoop(false);
       _AnimM9CargadorShoot->setLoop(false);
       _AnimBodyM9Shoot->setLoop(false);
       _AnimM9Shoot->setEnabled(true);
       _AnimM9_2Shoot->setEnabled(true);
       _AnimM9GatilloShoot->setEnabled(true);
       _AnimM9CargadorShoot->setEnabled(true);
       _AnimBodyM9Shoot->setEnabled(true);  
}

    void Player::loadAnimations(){

    //------GET ANIMATIONS SHOOT--------
   _AnimM9Shoot = nodeSceneMgr->getEntity(entM9->getName())->getAnimationState("shoot");
   _AnimM9_2Shoot = nodeSceneMgr->getEntity(entM92->getName())->getAnimationState("shoot");
   _AnimM9GatilloShoot = nodeSceneMgr->getEntity(entM9Gatillo->getName())->getAnimationState("shoot");
   _AnimM9CargadorShoot = nodeSceneMgr->getEntity(entM9Cargador->getName())->getAnimationState("shoot");
   _AnimBodyM9Shoot = nodeSceneMgr->getEntity(entBody->getName())->getAnimationState("shoot");
        
   
   //------GET ANIMATIONS RELOAD-------
   _AnimM9Reload = nodeSceneMgr->getEntity(entM9->getName())->getAnimationState("reload");
   _AnimM9_2Reload = nodeSceneMgr->getEntity(entM92->getName())->getAnimationState("reload");
   _AnimM9GatilloReload = nodeSceneMgr->getEntity(entM9Gatillo->getName())->getAnimationState("reload");
   _AnimM9CargadorReload = nodeSceneMgr->getEntity(entM9Cargador->getName())->getAnimationState("reload");
   _AnimBodyM9Reload = nodeSceneMgr->getEntity(entBody->getName())->getAnimationState("reload");
   
    }
    void Player::updateAnimations(){

   if (_AnimM9Shoot->getEnabled() && !_AnimM9Shoot->hasEnded())
    _AnimM9Shoot->addTime(deltaT);   
   if (_AnimM9_2Shoot->getEnabled() && !_AnimM9_2Shoot->hasEnded())
    _AnimM9_2Shoot->addTime(deltaT);  
   if (_AnimM9GatilloShoot->getEnabled() && !_AnimM9GatilloShoot->hasEnded())
    _AnimM9GatilloShoot->addTime(deltaT);
   if (_AnimM9CargadorShoot->getEnabled() && !_AnimM9CargadorShoot->hasEnded())
    _AnimM9CargadorShoot->addTime(deltaT);   
   if (_AnimBodyM9Shoot->getEnabled() && !_AnimBodyM9Shoot->hasEnded())
    _AnimBodyM9Shoot->addTime(deltaT);

   if (_AnimM9Reload->getEnabled() && !_AnimM9Reload->hasEnded())
    _AnimM9Reload->addTime(deltaT);   
   if (_AnimM9_2Reload->getEnabled() && !_AnimM9_2Reload->hasEnded())
    _AnimM9_2Reload->addTime(deltaT);
   if (_AnimM9GatilloReload->getEnabled() && !_AnimM9GatilloReload->hasEnded())
    _AnimM9GatilloReload->addTime(deltaT);   
   if (_AnimM9CargadorReload->getEnabled() && !_AnimM9CargadorReload->hasEnded())
    _AnimM9CargadorReload->addTime(deltaT);   
   if (_AnimBodyM9Reload->getEnabled() && !_AnimBodyM9Reload->hasEnded())
    _AnimBodyM9Reload->addTime(deltaT);

   
    }
    
void Player::update(Real elapsedTime, Ogre::Vector2 mRot,  OIS::KeyCode key, OIS::MouseButtonID mouse){

    deltaT = elapsedTime;
    updateAnimations();
    //FORMA 1
    /*
    rigidBody->setAngularVelocity(Vector3(0,mRot.x * deltaT * -3.0,0));
    rigidBody->setAngularVelocity(Vector3(0, 0, mRot.y * deltaT * -3.0)); */
    
    // FORMA 2 
    /*
    Vector3 vector = Vector3(0,0,0);
    if(mRot.y != 0)
    vector.z = mRot.y * deltaT * -3.0;
    if(mRot.x != 0)
    vector.y = mRot.x * deltaT * -3.0;
    
    rigidBody->setAngularVelocity(vector); */
    
    //FORMA 3
    //PARA HORIZONTAL
  /*  btTransform btT;
    rigidBody->getBulletRigidBody()->getMotionState()->getWorldTransform(btT);
    //------------------NEW ROTATION-------------------
    btQuaternion btNewQuaternion;
    btNewQuaternion.setEuler(btScalar(mRot.x * deltaT * -0.1), btScalar(0), btScalar(0));
    //-------------------------------------------------
    btT.setRotation(btNewQuaternion * rigidBody->getBulletRigidBody()->getOrientation());
    rigidBody->getBulletRigidBody()->setCenterOfMassTransform(btT); */
    rigidBody->setAngularVelocity(Vector3(0,mRot.x * deltaT * -1.0,0));
    //VERTICAL
    //nodePlayer->yaw(Ogre::Degree(-.1 * mRot.x), Ogre::Node::TS_WORLD);
    nodePlayer->pitch(Ogre::Degree(-.1 * mRot.y * deltaT), Ogre::Node::TS_LOCAL);
    //nodePlayer->yaw(Degree(-1 * deltaT * mRot.x));
    //nodePlayer->pitch(Degree(-1 * deltaT * mRot.y));
  //  nodePlayer->pitch(Degree(3 * deltaT * mRot.y));
 //   nodeCamera->pitch(Degree(3 * deltaT * mRot.y));
   //FORMA 4
//    Ogre::Quaternion actual = rigidBody->getWorldOrientation();
//    rigidBody->setOrientation( actual * Ogre::Quaternion( Ogre::Degree( -3 * deltaT * mRot.x ), Ogre::Vector3::UNIT_X ) );
    //FORMA 5
  /*  
    Ogre::Quaternion actual = rigidBody->getWorldOrientation();
    btRigidBody* rigid = rigidBody->getBulletRigidBody();
    btTransform tr;
    tr.setOrigin(rigid->getCenterOfMassPosition());
    tr.setRotation(OgreBulletCollisions::convert(actual * Ogre::Quaternion( Ogre::Degree( -3 * deltaT * mRot.x ), Ogre::Vector3::UNIT_X )));
    rigid->getMotionState()->setWorldTransform(tr); 

    */
    
    if(test == 0) {timeDead+=deltaT;  } //Se cuenta los segundos de muerto
    deadPlayer();
    if(test == 100) {timeBlood = 0;} //Si su salud es 100, deja de sangrar
    if(test<100){timeBlood+=deltaT;} //Si su salud es mejor a 100, se empieza a contar el tiempo de sangre
    if(timeBlood>10.0 && test>0 && test<=99) {test+=1;} //Si esta sangrando hace mas de 6 segundos, empieza a curarse
    if(test<100 && !GameManager::getSingletonPtr()->_heridoFX->isPlaying()) 
    {
        GameManager::getSingletonPtr()->_heridoFX->play();
        _panelP->show();
    }
    if(test == 100 && GameManager::getSingletonPtr()->_heridoFX->isPlaying()) 
    {
        GameManager::getSingletonPtr()->_heridoFX->stop();
        _panelP->hide();
    }
      if(test<100 && test>=80) {_panelP->setMaterialName("panelHerido1");}
      else if(test<=80 && test>=60) {_panelP->setMaterialName("panelHerido2");}
      else if(test<=60) {_panelP->setMaterialName("panelHerido3");}
 /*   if(mRot.y != 0) //Horizontal
    {
            btTransform btT;
    rigidBody->getBulletRigidBody()->getMotionState()->getWorldTransform(btT);
    //------------------NEW ROTATION-------------------
    btQuaternion btNewQuaternion;
    btNewQuaternion.setEuler(btScalar(0), btScalar(0), btScalar(mRot.y * deltaT * -0.1));
    //-------------------------------------------------
    btT.setRotation(btNewQuaternion * rigidBody->getBulletRigidBody()->getOrientation());
    rigidBody->getBulletRigidBody()->setCenterOfMassTransform(btT);
//    rigidBody->getSceneNode()->_setDerivedOrientation(rigidBody->getCenterOfMassOrientation());
    } 
    if(mRot.y != 0)
    {
    btTransform btT;
    rigidBody->getBulletRigidBody()->getMotionState()->getWorldTransform(btT);
    //------------------NEW ROTATION-------------------
    btQuaternion btNewQuaternion;
    btNewQuaternion.setEuler(btScalar(0), btScalar(0), btScalar(mRot.y * deltaT * -0.1 ));
    //-------------------------------------------------
    btT.setRotation(btNewQuaternion * rigidBody->getBulletRigidBody()->getOrientation());
    rigidBody->getBulletRigidBody()->setCenterOfMassTransform(btT);
    rigidBody->getSceneNode()->_setDerivedOrientation(rigidBody->getCenterOfMassOrientation());
    } */
   // rigidBody->getBulletRigidBody()->setAngularVelocity(btVector3(0,0, mRot.y * deltaT * -3.0));
  //  rigidBody->getBulletRigidBody()->setAngularVelocity(btVector3(0,mRot.x * deltaT * -3.0,0));
//    rigidBody->getBulletRigidBody()->setAngularVelocity(btVector3(0, 0, ));
   // rigidBody->getBulletRigidBody()->setAngularVelocity(btVector3(0, 0, ));
    //nodePlayer->pitch(Ogre::Degree());
   // rigidBody->getBulletRigidBody()->setAngularVelocity(btVector3(mRot.y * deltaT * -5.0, 0, 0));
    /*if(_panelP->isVisible()){
        timeBlood+=deltaT;
    }
    else{
        timeBlood = 0;
    }
    if(timeBlood>0.35)
    {
        _panelP->setMaterialName("panelHerido2");
    }
    if(timeBlood>0.70)
    {
        _panelP->setMaterialName("panelHerido3");
    }
    if(timeBlood>4.5)
    {
        _panelP->setMaterialName("panelHerido2");
    }
    if(timeBlood>5.5)
    {
        _panelP->setMaterialName("panelHerido1");
    }
    if(timeBlood>6.5)
    {
        timeBlood = 0;
        setOpacityOverlay(0);
    }*/
    
    if(key == OIS::KC_W) {moveW();}
    if(key == OIS::KC_S) {moveS();}
    if(key == OIS::KC_A) {moveA();}
    if(key == OIS::KC_D) {moveD();}
    if(key == OIS::KC_Z) {stop();}
   // else if(key == OIS::KC_R) {reload();}
   // if(mouse == OIS::MB_Left) {shot();}
}  


void Player::moveW(){
 if (rigidBody) {  
      if (!rigidBody->isStaticObject()) { 
       rigidBody->enableActiveState ();
       Vector3 direction = camera->getDerivedDirection();
       direction.y = 0;
       direction.normalisedCopy();
       rigidBody->setLinearVelocity( direction * 160.0 * deltaT);
        
      }
    
    } 
}

void Player::moveS(){
        if (rigidBody) {  
      if (!rigidBody->isStaticObject()) { 
       rigidBody->enableActiveState ();
       Vector3 direction = camera->getDerivedDirection();
       direction.y = 0;
       direction.normalisedCopy();
       rigidBody->setLinearVelocity( direction * -160.0 * deltaT);
        
      }
    
    }
}

void Player::moveA(){
        if (rigidBody) {  
      if (!rigidBody->isStaticObject()) { 
	rigidBody->enableActiveState ();
       Vector3 direction = camera->getDerivedDirection();
       direction.y = 0;
       direction = direction.crossProduct( Vector3::UNIT_Y );
       direction = direction.normalisedCopy();
        rigidBody->setLinearVelocity( direction * -160.0 * deltaT);
            }
      
        }   
}

void Player::moveD(){
        if (rigidBody) {  
      if (!rigidBody->isStaticObject()) { 
	rigidBody->enableActiveState ();
       Vector3 direction = camera->getDerivedDirection();
       direction.y = 0;
       direction = direction.crossProduct( Vector3::UNIT_Y );
       direction = direction.normalisedCopy();
        rigidBody->setLinearVelocity( direction * 160.0 * deltaT);
            }
      
        }   
}

void Player::stop(){
        if (rigidBody) {  
      if (!rigidBody->isStaticObject()) { 
	rigidBody->enableActiveState ();
        rigidBody->setLinearVelocity(Vector3(0,0,0));
            }
      
        }   
}

void Player::shot(){

       GameManager::getSingletonPtr()->_akShootFX->play();
       //Part of animation
       _AnimM9Shoot->setTimePosition(0);
       _AnimM9_2Shoot->setTimePosition(0);
       _AnimM9GatilloShoot->setTimePosition(0);
       _AnimM9CargadorShoot->setTimePosition(0);
       _AnimBodyM9Shoot->setTimePosition(0);
       _AnimM9Shoot->setLoop(false);
       _AnimM9_2Shoot->setLoop(false);
       _AnimM9GatilloShoot->setLoop(false);
       _AnimM9CargadorShoot->setLoop(false);
       _AnimBodyM9Shoot->setLoop(false);
       _AnimM9Shoot->setEnabled(true);
       _AnimM9_2Shoot->setEnabled(true);
       _AnimM9GatilloShoot->setEnabled(true);
       _AnimM9CargadorShoot->setEnabled(true);
       _AnimBodyM9Shoot->setEnabled(true);  

    
       
}

void Player::reload(){

    
       GameManager::getSingletonPtr()->_m9ReloadFX->play();     //Effects shoot
       _AnimM9Reload->setTimePosition(0);
       _AnimM9_2Reload->setTimePosition(0);
       _AnimM9GatilloReload->setTimePosition(0);
       _AnimM9CargadorReload->setTimePosition(0);
       _AnimBodyM9Reload->setTimePosition(0);
       _AnimM9Reload->setLoop(false);
       _AnimM9_2Reload->setLoop(false);
       _AnimM9GatilloReload->setLoop(false);
       _AnimM9CargadorReload->setLoop(false);
       _AnimBodyM9Reload->setLoop(false);
       _AnimM9Reload->setEnabled(true);
       _AnimM9_2Reload->setEnabled(true);
       _AnimM9GatilloReload->setEnabled(true);
       _AnimM9CargadorReload->setEnabled(true);
       _AnimBodyM9Reload->setEnabled(true);
       
}

void Player::deadPlayer(){

    if(timeDead>0 && timeDead<2) {
    rigidBody->getBulletRigidBody()->applyTorque(OgreBulletCollisions::convert(Ogre::Vector3(0,0,5000)));
    }
    if(timeDead>=1.6){
        nodeSceneMgr->destroyAllLights();
    }
    if(timeDead>=2.6){
        nodeSceneMgr->setAmbientLight(Ogre::ColourValue(0,0,0));}
   


}
    

void Player::reduce(Ogre::int32 i){
    if((test-i) <= 0) test = 0; //Si la resta es negativa, le asigno 0
    else test= test - i; //En caso contrario resto.
}

bool Player::checkShoot(){
    return !_AnimM9Reload->getEnabled() || _AnimM9Reload->hasEnded();
}


int Player::decideHurt(Ogre::Real y){
        int hurt = 0;     
        if(y>0.150) {
        hurt = 40 + rand() % (100 - 40); //Si el disparo fue arriba de la cintura es minimo de 40
        }else{
        hurt = 10 + rand() % (39 - 10); //Si es abajo puede ser de 10 a 39
        }
        return hurt; 
}

void Player::overlayHerido(){

    double width = 1600.0;
    double height = 900.0;
    Ogre::OverlayManager *_overlayMgr = Ogre::OverlayManager::getSingletonPtr();
    _overlay = _overlayMgr->create("overlayPlayer");
    Ogre::OverlayContainer* _panel = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerPlayer"));
    _panel->setMetricsMode(Ogre::GMM_PIXELS);
    _panel->setPosition(0, 0);
    _panel->setDimensions(width, height);
    _panel->setMaterialName("panelHerido3"); // Optional background material
 
    _overlay->add2D(_panel);
    _panelP = _panel;
    _overlay->show(); 
    
    
}


Player::Player(const Player& orig) {
}

Player::~Player() {
}





#include "Weapon.h"

Weapon::Weapon() {
  ammo_total = 0; //Total de balas disponibles
  capacity_charger = 0; //Capacidad del cargador
  ammo_charger = 0; //Balas actualmente en el arma
  id = 0;
}

Weapon::Weapon(const Weapon& orig) {
}

Weapon::~Weapon() {
}

 void Weapon::shoot(void) {
    if (ammo_charger > 0){
        ammo_charger--;
        id++;
    }
  }

  bool Weapon::is_empty_total(void) {
    return ammo_total == 0;
  }
   bool Weapon::is_empty_charger(void) {
    return ammo_charger == 0;
  }

  int Weapon::get_ammo_total(void) {
    return ammo_total;
  }
  int Weapon::get_ammo_charger() {
    return ammo_charger;
  }

  void Weapon::add_ammo_total(int amount) {
      ammo_total+=amount;
  }
  void Weapon::reload_charger() {
      if(ammo_total>capacity_charger){
     int aux =  capacity_charger - ammo_charger;
     if(ammo_total - aux > 0){
     ammo_total= ammo_total-aux;
     ammo_charger= ammo_charger+aux;}
      }else{
          ammo_charger = ammo_total + ammo_charger;
          ammo_total = 0;
          
      }
  }
  void Weapon::setCapacity(int cant) {
      capacity_charger = cant;
  }
  int Weapon::getCapacity() {
      return capacity_charger;
  }
  int Weapon::getIdBullet(){
      return id;
  }

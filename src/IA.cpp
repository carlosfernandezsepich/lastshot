
#include "IA.h"
#include "Weapon.h"


IA::IA(std::vector<Terrorist*> *pTerrorists, RigidBody *pPlayer, OgreBulletDynamics::DynamicsWorld *pWorld, Weapon *pWeapon) {
    terrorists = pTerrorists;
    player = pPlayer;
    world = pWorld;
    notifyAllTerrorists = false;
    auxT = 0;
    weapon = pWeapon;
    limitCharger = 10;
    limitTotal = 30;
    
    
}

IA::IA(const IA& orig) {
}

IA::~IA() {
}

void IA::updateWorld(Ogre::Real pDeltaT){
    deltaT = pDeltaT;
    auxT+=deltaT;
    std::cout<<"Hay "<<terrorists->size()<<" terroristas vivos"<<std::endl;
    for(int i = 0; i<terrorists->size(); i++)
    {
        terrorists->at(i)->updateAnimations(pDeltaT);
    switch(terrorists->at(i)->Informar()) 
        {
            case vigilando:
            {
                Vector3 positionPlayer = player->getSceneNode()->_getDerivedPosition();
                Vector3 positionTerrorist = terrorists->at(i)->getRigidBody()->getSceneNode()->_getDerivedPosition();
                Ogre::Real distance = positionTerrorist.distance(positionPlayer);
                if(distance<Ogre::Real(40) || weapon->get_ammo_total()<limitTotal || weapon->get_ammo_charger()<limitCharger)
                {
                    limitTotal = weapon->get_ammo_total();
                    limitCharger = weapon->get_ammo_charger();
                    terrorists->at(i)->stop(); //Detengo la fisica
                    terrorists->at(i)->stopAllAnimations(); //Detengo las animacione
                    terrorists->at(i)->ChangeState('a'); //Cambio el estado
                }
                
                break;
            }
            case atentando:
            {
                //Aquel enemigo que se encuentre herido, parado y con una altura inferior a 1, va a salir corriendo por su vida

                if(terrorists->at(i)->getLife() == Ogre::int32(0)) {terrorists->at(i)->stopAllAnimations(); terrorists->at(i)->ChangeState('m');}
                if(terrorists->at(i)->getLife() <=Ogre::int32(50) && terrorists->at(i)->getLife() > Ogre::int32(0) 
                        && terrorists->at(i)->getId()%2 == 0)
                {
                    terrorists->at(i)->changeStateToHuyendoAnims();
                    terrorists->at(i)->ChangeState('h');
                } 
                if(terrorists->at(i)->getTimeNoWatchPlayer() >=27.0){
                     terrorists->at(i)->changeStateToHuyendoAnims();
                    terrorists->at(i)->ChangeState('v');
                }
                break;
            }
            case huyendo:  
            {
                if(terrorists->at(i)->getLife() == Ogre::int32(0)) {terrorists->at(i)->stop(); terrorists->at(i)->changeStateToHuyendoAnims(); terrorists->at(i)->ChangeState('m');}
                else{
                Ogre::Vector3 pos = OgreBulletCollisions::convert(terrorists->at(i)->getRigidBody()->getBulletRigidBody()->getCenterOfMassPosition());
                Ogre::Ray rMove(pos, OgreBulletCollisions::convert(terrorists->at(i)->getRigidBody()->getBulletRigidBody()->getOrientation()) * Ogre::Vector3::UNIT_Z);
                OgreBulletCollisions::CollisionClosestRayResultCallback cQueryMove = CollisionClosestRayResultCallback (rMove, world, 15);
                world->launchRay(cQueryMove);
                if (cQueryMove.doesCollide())  terrorists->at(i)->rotate("right");
                else terrorists->at(i)->run(true, terrorists->at(i)->createRay());
                }
                break;
                
            }
            case muerto:
            {
                //Si ya finalizo su animacion de muerte
                if(terrorists->at(i)->checkDead() == true)
                {
                    terrorists->at(i)->desactivePhysical();
                    terrorists->erase(terrorists->begin() + i); //Lo elimino de la lista de enemigos                  
                }
                break;
             
            }
            //default: std::cout<<"entra al default"<<std::endl;

        }
        
    }
   }
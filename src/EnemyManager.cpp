/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EnemyManager.cpp
 * Author: marcado
 * 
 * Created on 26 de mayo de 2017, 12:54
 */

#include "EnemyManager.h"

EnemyManager::EnemyManager() {
}

EnemyManager::EnemyManager(const EnemyManager& orig) {
}

EnemyManager::~EnemyManager() {
}
void EnemyManager::addEnemyList(Enemy *pEnemy){
    _enemies.push_back(pEnemy);
    //cout<< "Bala size: " << _bulletsList.size()<<endl;
}

void EnemyManager::deleteEnemyList(Enemy *pEnemy){
    for(int i=0; i<_enemies.size(); i++)
    {
        if(_enemies.at(i)->getId() == pEnemy->getId())
        {
            _enemies.erase(_enemies.begin() + i);
            // Deletes the second element (vec[1])
        }
    }
}
vector<Enemy*> EnemyManager::getEnemyList(){
    return _enemies;
}
void EnemyManager::changeState(Enemy* pEnemy){
    pEnemy->setState(!pEnemy->getState());
}



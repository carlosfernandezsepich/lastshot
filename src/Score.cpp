/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Score.cpp
 * Author: esi
 * 
 * Created on 15 de febrero de 2017, 16:45
 */

#include "Score.h"

Score::Score() {
    _score = 0;
    _name = "";
}

Score::Score( int pScore, string pName ){
    _score = pScore;
    _name = pName;
}

Score::Score(const Score& orig) {
}

Score::~Score() {
    _score = 0;
    _name = "";
}

void Score::setName( string const &pName){
    _name = pName;
}

string Score::getName() const{
    return _name;
}

void Score::setScore( int const &pScore){
    _score = pScore;
}

int Score::getScore() const{
    return _score;
}



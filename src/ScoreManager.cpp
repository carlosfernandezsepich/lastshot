/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ScoreManager.cpp
 * Author: esi
 * 
 * Created on 15 de febrero de 2017, 16:46
 */

#include "ScoreManager.h"
#include <ctype.h>
#include <sstream>
#include <string>

using namespace std;

string ScoreManager::file_name = "scores.txt";
ifstream ScoreManager::fs;


ScoreManager::ScoreManager() {}

bool ScoreManager::openFile(const string name, const bool doCheck){

    bool valid_file = true;

    if (doCheck){

        valid_file = false;

        struct stat s;
        if( stat((name).c_str(),&s) == 0 ){
            if( s.st_mode & S_IFDIR ){
                cout<<"The path is a directory. "<<endl;
            }
            else if( s.st_mode & S_IFREG ){
                valid_file = true;
            }else{
                cout<<"The path is not file or directory. "<<endl;
                cout<<"else "<<endl;
            }
        }else{
            cout<<"An error happened as trying to identify the path. "<<endl;
        }
    }

    if (!fs.is_open() && valid_file){
        const char *title = name.c_str();
        fs.open(title);
    }

    return fs.is_open();
}

void ScoreManager::closeFile(){
    if (fs.is_open()){
        fs.close();
    }
}

vector<Score> ScoreManager::getScores(){
    
    vector<Score> list;
    Score* reg;
    
    file_name = "scores.txt";   
    
    if ( ScoreManager::openFile(ScoreManager::file_name, true) ){

        char c;
        string word = "";
        string name = "";
        string score = "";
        int result;

        while ( fs.get(c)){

            if ( c == '\n'){
                name = word;
                word = "";
                reg = new Score(result, name); // stringstream used for the conversion initialized with the contents of Text
                list.push_back(*reg);
                delete reg;
                
            }else if ( c == ';'){
                score = word;
                word = "";
                stringstream convert(score);
                convert >> result;

            }else{
                word = word + string(1, toupper(c)) ;
            }

        }

        fs.clear();
        fs.seekg(0,fs.beg); // Before leaving we set the position on the first character.

        closeFile();
 
    }
    
    return list;
    
}

vector<string> ScoreManager::getListScores(){
    ifstream read;
    vector<Score> scores;
    Score unScore;
    string line, name, score;
    vector<string> myVector;
    file_name = "scores.txt";   
    
    if (openFile(file_name, false)){
        closeFile();
    }   
    read.open("scores.txt", ios::in);
    
    while(getline(read, line))
    {
        myVector.push_back(line);
    }
    closeFile();

    return myVector;
    
}

void ScoreManager::write_file(string pName, int pScore ){
    
    file_name = "scores.txt";

    if (openFile(file_name, false)){
        closeFile();
    }
    
    
    ofstream fl("scores.txt", ios::app); //Abro el archivo y le digo que inserte al final
    ofstream fltemp("temp.txt", ios::app); //Abro el archivo y le digo que inserte al final
    if (fl.is_open() && fltemp.is_open()){
    // ya se puede leer ...
        fl<<pScore<<" "<<pName<<endl; //Guardo el actual
        vector<string> actual = orderList(getListScores()); //Obtengo todos ordenados
         for(int i = 0; i<actual.size(); i++){
            fltemp<<actual[i]<<endl; //Escrito todos en el temporal
        }
        
    fl.close();    //Cierro el archivo score
    fltemp.close(); //Cierro el archivo temp
    remove("scores.txt"); //Elimino el archivo
    rename("temp.txt", "scores.txt");
    }else
    {
        cout<<"Error al abrir el archivo"<<endl;;
    }
}

vector<string> ScoreManager::orderList(vector<string> pList){

    int i, j;
    string temp;
    for (i=1; i<pList.size(); i++){
          for (j=0 ; j<pList.size() - 1; j++){
               if (getOnlyInt(pList[j]) < getOnlyInt(pList[j+1])){
                    temp = pList[j];
                    pList[j] = pList[j+1];
                    pList[j+1] = temp;}
          }
    }
    return pList;
}

int ScoreManager::getOnlyInt(string pRow){
    int row, i;
    string auxRow;
    for(i = 0; i<pRow.length(); i++){
        if(isdigit(pRow[i])){
            auxRow+=pRow[i];
        }
    }
    row = atoi(auxRow.c_str());
    return row;
}

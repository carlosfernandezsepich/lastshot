#include "MenuState.h"
#include "PlayState.h"
#include "ScreenLoadingState.h"
#include <string>
#include <stdlib.h>

#include <OGRE/Overlay/OgreOverlayElement.h>

#include <iostream> // std::cout
#include <thread> // std::thread, std::this_thread::sleep_for
#include <chrono> // std::chrono::seconds
using namespace std;

template<> MenuState* Ogre::Singleton<MenuState>::msSingleton = 0;

using namespace CEGUI;
void
MenuState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _root->getAutoCreatedWindow()->removeAllViewports();
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  double width = _viewport->getActualWidth();
  double height = _viewport->getActualHeight();
  _camera->setAspectRatio(width / height);
  // Nuevo background colour.
  _viewport->setBackgroundColour(Ogre::ColourValue(0.5, 0.5, 0.5));
  OverlayManager::getSingletonPtr()->destroyAllOverlayElements();
  OverlayManager::getSingletonPtr()->destroyAll();
  GameManager::getSingletonPtr()->getMenuStateTrack()->play();     //Play main music

  //_viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 1.0));
  _exitGame = false;
  createMenu();
  createWallPaper();
  _myVector = ScoreManager::getListScores();
  
  
}

void
MenuState::exit()
{
  OverlayManager::getSingletonPtr()->destroyAllOverlayElements();
  OverlayManager::getSingletonPtr()->destroyAll();
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();
  deleteMenu();
}

void
MenuState::pause ()
{
    _sheet->setEnabled(false);          //CEGUI Disabled
    _sheet->setVisible(false);          //CEGUI Invisible
  //  GameManager::getSingletonPtr()->getTrackMenuPtr()->stop();
}

void
MenuState::resume ()
{
    _sheet->setEnabled(true);          //CEGUI Enabled
    _sheet->setVisible(true);          //CEGUI Visible
   // GameManager::getSingletonPtr()->getTrackMenuPtr()->play();
}

bool
MenuState::frameStarted
(const Ogre::FrameEvent& evt) 
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(evt.timeSinceLastFrame);  
  return true;
}

bool
MenuState::frameEnded
(const Ogre::FrameEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(evt.timeSinceLastFrame);  
  if (_exitGame)
    return false;
  
  return true;
}

void
MenuState::keyPressed
(const OIS::KeyEvent &e)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(e.key));
  CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(e.text); 
}

void
MenuState::keyReleased
(const OIS::KeyEvent &e )
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(static_cast<CEGUI::Key::Scan>(e.key));     
  if (e.key == OIS::KC_ESCAPE) {
    _exitGame = true;
  }
}

void
MenuState::mouseMoved
(const OIS::MouseEvent &e)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel); 
}

void
MenuState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
 CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void
MenuState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton MenuState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

MenuState*
MenuState::getSingletonPtr ()
{
return msSingleton;
}

MenuState&
MenuState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}
void MenuState::createMenu(){
    
  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

  CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
  //CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("AlfiskoSkin/MouseArrow");

  // Let's make the OS and the CEGUI cursor be in the same place
  CEGUI::Vector2f mousePos = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();  
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(-mousePos.d_x,-mousePos.d_y);
 
  //Sheet
  _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","MenuPrincipalSheet");
  
  //New Game button
  CEGUI::Window* newGameButton = CEGUI::WindowManager::getSingleton().createWindow("AlfiskoSkin/Button","MenuPrincipal/NewGameButton");
  newGameButton->setText("Nueva Partida");
  newGameButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  newGameButton->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.83,0),CEGUI::UDim(0.3,0)));
  newGameButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&MenuState::play, this));
  
   //Score button
  CEGUI::Window* scoreButton = CEGUI::WindowManager::getSingleton().createWindow("AlfiskoSkin/Button","MenuPrincipal/ScoreButton");
  scoreButton->setText("Puntaje");
  scoreButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  scoreButton->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.83,0),CEGUI::UDim(0.4,0)));
  scoreButton->subscribeEvent(CEGUI::PushButton::EventClicked,
  			     CEGUI::Event::Subscriber(&MenuState::score, this));
  
    //Config button
  CEGUI::Window* configButton = CEGUI::WindowManager::getSingleton().createWindow("AlfiskoSkin/Button","MenuPrincipal/ConfigButton");
  configButton->setText("Ajustes");
  configButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  configButton->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.83,0),CEGUI::UDim(0.5,0)));
  configButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MenuState::configuration, this));
  
  //Credit button
  CEGUI::Window* creditButton = CEGUI::WindowManager::getSingleton().createWindow("AlfiskoSkin/Button","MenuPrincipal/CreditButton");
  creditButton->setText("Creditos");
  creditButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  creditButton->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.83,0),CEGUI::UDim(0.6,0)));
  creditButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&MenuState::credits, this));
  
    //Quit button
  CEGUI::Window* quitButton = CEGUI::WindowManager::getSingleton().createWindow("AlfiskoSkin/Button","MenuPrincipal/QuitButton");
  quitButton->setText("Salir");
  quitButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  quitButton->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.83,0),CEGUI::UDim(0.7,0)));
  quitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&MenuState::quit, this));
  //Attaching buttons
  _sheet->addChild(newGameButton);
  _sheet->addChild(scoreButton);
  _sheet->addChild(configButton);
  _sheet->addChild(creditButton);
  _sheet->addChild(quitButton);
  CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_sheet);
  
  
}

void MenuState::deleteMenu(){
    CEGUI::WindowManager::getSingleton().destroyWindow(_sheet); 
    _sheet->destroy();
}

bool MenuState::play(const CEGUI::EventArgs &e)
{
    deleteMenu();
    createWriteName();
    return true;
}

bool MenuState::score(const CEGUI::EventArgs &e)
{
    deleteMenu();
    createMenuScore();
    return true;
}

bool MenuState::configuration(const CEGUI::EventArgs &e)
{
    deleteMenu();
    createConfigurationMenu();
    return true;
}
bool MenuState::credits(const CEGUI::EventArgs& e)
{
    deleteMenu();
    createCreditsMenu();
}
bool MenuState::quit(const CEGUI::EventArgs &e)
{
  _exitGame = true;
  return true;
}


bool MenuState::continueGame(const CEGUI::EventArgs &e)
{

  OverlayManager::getSingletonPtr()->destroyAllOverlayElements();
  OverlayManager::getSingletonPtr()->destroyAll();  
  deleteMenu(); //Elimino el menu CEGUI  
  string name = static_cast<std::ostringstream*>(&(std::ostringstream() <<editBoxName->getText()))->str();
  GameManager::getSingletonPtr()->setNamePlayer(name); //Guardo el nombre ingreso en el jugador
  changeState(ScreenLoadingState::getSingletonPtr());
  // sleep(1);
  // createScreenLoading(); 
  // sleep(3); 
  // changeState(PlayState::getSingletonPtr());
   // OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
   // Ogre::Overlay *_overlay = _overlayMgr->getByName("overlayMenuState");
   // _overlay->getChild("containerMenuState")->setMaterialName("ScreenLoading2");
    
    /*
   string name = static_cast<std::ostringstream*>(&(std::ostringstream() <<editBoxName->getText()))->str();
   GameManager::getSingletonPtr()->setNamePlayer(name);
   GameManager::getSingletonPtr()->getTrackMenuPtr()->stop();
   changeState(PlayState::getSingletonPtr());
   */
    
   	// Create the thread and start work
 //   assert(!mThread);
 //   mThread = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&MenuState::menu1, this))); 
 //  createScreenLoading();  
  
    //HILOS
 // std::cout << "Spawning 3 threads...\n";
 // std::thread t1(&MenuState::menu1, this, 1)
   //  std::thread t3(&MenuState::menu3, this, 3);
   //  std::thread t2(&MenuState::menu2, this, 2);

 // std::cout << "Done spawning threads. Now waiting for them to join:\n";
 // t1.join();
 // t3.join();
 // t2.join();
 // std::cout << "All threads joined!\n";
    
    
  return true;
}


 void MenuState::createMenuScore(){ 
   CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

  CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
  CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");

  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("AlfiskoSkin/MouseArrow");

  // Let's make the OS and the CEGUI cursor be in the same place
  CEGUI::Vector2f mousePos = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();  
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(-mousePos.d_x,-mousePos.d_y);

  // load all the fonts (if they are not loaded yet)
  //CEGUI::FontManager::getSingleton().createAll("*.font", "Fonts");
    
  //Sheet
  _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Sheet");

  //Config Window
  CEGUI::Window* _windowScore = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("scoreStateMenu.layout");
  int i = 0;
  string nameRow = "";
  string contentRow = "";     
  for(i; i<_myVector.size(); i++)
  {
    nameRow = "row" + static_cast<std::ostringstream*>(&(std::ostringstream() << i))->str();
     contentRow = _myVector.at(i);
    _windowScore->getChild(nameRow)->setText(contentRow);
  }
  
  //Setting Text!


  CEGUI::Window* exitButton = _windowScore->getChild("ReturnButton");
  exitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&MenuState::returnToMenu, this));
  _sheet->addChild(_windowScore);
  CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_sheet);
}
void MenuState::createCreditsMenu(){

      //CEGUI
  //renderer = &CEGUI::OgreRenderer::bootstrapSystem();
  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

  CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
  //CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");

  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("AlfiskoSkin/MouseArrow");

  // Let's make the OS and the CEGUI cursor be in the same place
  CEGUI::Vector2f mousePos = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();  
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(-mousePos.d_x,-mousePos.d_y);

  // load all the fonts (if they are not loaded yet)
  //CEGUI::FontManager::getSingleton().createAll("*.font", "Fonts");
    
  //Sheet
  _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Sheet");

  //Config Window
  CEGUI::Window* formatWin = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("stringFormat.layout");
  
  //Setting Text!
  formatWin->getChild("Lprogramming")->setText("Programming Lead: Carlos Fernandez");
  formatWin->getChild("Ldesign")->setText("Design Lead: Rolando Rolon.");
  formatWin->getChild("Laudio")->setText("Audio Lead: Carlos Fernandez");
  formatWin->getChild("Lanimation")->setText("Animation Lead: Rolando Rolon.");
  formatWin->getChild("Lquality")->setText("Quality Assurance Lead: Carlos Fernandez");
   
  //Exit Window
  CEGUI::Window* exitButton = formatWin->getChild("ReturnButton");
  exitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&MenuState::returnToMenu, this));
  //Attaching buttons
  _sheet->addChild(formatWin);
  CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_sheet);
}
void MenuState::createConfigurationMenu(){
    
    //CEGUI
  //renderer = &CEGUI::OgreRenderer::bootstrapSystem();
  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

  CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
  CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");

  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("AlfiskoSkin/MouseArrow");

  // Let's make the OS and the CEGUI cursor be in the same place
  CEGUI::Vector2f mousePos = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();  
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(-mousePos.d_x,-mousePos.d_y);

  // load all the fonts (if they are not loaded yet)
  //CEGUI::FontManager::getSingleton().createAll("*.font", "Fonts");
    
  //Sheet
  _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","SheetConfig");

  //Config Window
  CEGUI::Window* formatWin = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("configStateMenu.layout");
  
  //Setting Text!
    formatWin->getChild("W")->setText("Avanzar: W");
    formatWin->getChild("S")->setText("Retroceder: S");
    formatWin->getChild("A")->setText("Izquierda: A");
    formatWin->getChild("D")->setText("Derecha: D");
    formatWin->getChild("R")->setText("Recargar: R");
            
  formatWin->getChild("IzqClic")->setText("Disparar: Clic izquierdo");
  formatWin->getChild("ESC")->setText("Regresar al menu: ESC");
  formatWin->getChild("P")->setText("Pausa: P o ESC");
  
  /*
  CEGUI::Window* editBoxName = formatWin->getChild("EditBoxName");
  editBoxName->setSize(CEGUI::USize(CEGUI::UDim(0.5,0),CEGUI::UDim(0.05,0)));
  editBoxName->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.2,0),CEGUI::UDim(0.8,0)));
  editBoxName->setText("Unknown");
  editBoxName->activate();
  
  
  CEGUI::Window* applyButton = CEGUI::WindowManager::getSingleton().createWindow("AlfiskoSkin/Button","MenuPrincipal/ScoreButton");
  applyButton->setText("Apply");
  applyButton->setSize(CEGUI::USize(CEGUI::UDim(0.10,0),CEGUI::UDim(0.05,0)));
  applyButton->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.7,0),CEGUI::UDim(0.8,0)));
  applyButton->subscribeEvent(CEGUI::PushButton::EventClicked,
  			     CEGUI::Event::Subscriber(&MenuState::apply, this));
  
  CEGUI::Window* restorerButton = CEGUI::WindowManager::getSingleton().createWindow("AlfiskoSkin/Button","MenuPrincipal/ScoreButton");
  restorerButton->setText("Restorer");
  restorerButton->setSize(CEGUI::USize(CEGUI::UDim(0.10,0),CEGUI::UDim(0.05,0)));
  restorerButton->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.8,0),CEGUI::UDim(0.8,0)));
  restorerButton->subscribeEvent(CEGUI::PushButton::EventClicked,
  			     CEGUI::Event::Subscriber(&MenuState::restorer, this));
  
   */
  //Exit Window
  CEGUI::Window* exitButton = formatWin->getChild("ReturnButton");
  exitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&MenuState::returnToMenu, this));
  //Attaching buttons
//  formatWin->addChild(applyButton);
//  formatWin->addChild(restorerButton);
  _sheet->addChild(formatWin);
  CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_sheet);
}

void MenuState::createWriteName(){
  //CEGUI
  
  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

  CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
  CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("AlfiskoSkin/MouseArrow");

  // Let's make the OS and the CEGUI cursor be in the same place
  CEGUI::Vector2f mousePos = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();  
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(-mousePos.d_x,-mousePos.d_y);
 
  //Sheet
    // load all the fonts (if they are not loaded yet)
  //CEGUI::FontManager::getSingleton().createAll("*.font", "Fonts");

  //Sheet
  _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","MenuSelectLvlSheet");

  
  CEGUI::Window* selLvl = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("selLvl.layout");
  

  
  editBoxName = selLvl->getChild("EditBoxName");
  editBoxName->setSize(CEGUI::USize(CEGUI::UDim(0.8,0),CEGUI::UDim(0.2,0)));
  editBoxName->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.1,0),CEGUI::UDim(0.2,0)));
  editBoxName->setText("Unknown");
  editBoxName->activate();
  
  CEGUI::Window* btnContinue = CEGUI::WindowManager::getSingleton().createWindow("AlfiskoSkin/Button","MenuSelectLvl/LvlNoob");
  btnContinue->setText("Continuar");
  btnContinue->setSize(CEGUI::USize(CEGUI::UDim(0.80,0),CEGUI::UDim(0.2,0)));
  btnContinue->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.1,0),CEGUI::UDim(0.70,0)));
  btnContinue->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&MenuState::continueGame, this));

  //Attaching buttons
  selLvl->addChild(btnContinue);
  _sheet->addChild(selLvl);
  CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_sheet);
}

void MenuState::createWallPaper(){
    
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Ogre::Overlay *_overlay = _overlayMgr->create("overlayMenuState");
    _panel = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerMenuState"));
    _panel->setMetricsMode(Ogre::GMM_PIXELS);
    _panel->setPosition(0, 0);
    _panel->setDimensions(width, height);
    _panel->setMaterialName("Menu"); // Optional background material
    _overlay->add2D(_panel);
 
    _overlay->show();
  
}

void MenuState::createScreenLoading(){
    
    
    OverlayManager::getSingletonPtr()->destroyAllOverlayElements();
    OverlayManager::getSingletonPtr()->destroyAll();
    deleteMenu();
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
    Ogre::Overlay *_overlay = _overlayMgr->create("overlayScreenLoading");
    _panel = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerScreenLoading"));
    _panel->setMetricsMode(Ogre::GMM_PIXELS);
    _panel->setPosition(0, 0);
    _panel->setDimensions(width, height);
    _panel->setMaterialName("ScreenLoading1"); // Optional background material
    _overlay->add2D(_panel);
 
    _overlay->show();
  
}



bool MenuState::returnToMenu(const CEGUI::EventArgs &e)
{
  //returnToMenu code  
  //  changeState(MenuState::getSingletonPtr());
    deleteMenu();
    createMenu();
  return true;
}

void MenuState::menu1() 
{
   OverlayManager *_overlayMgr = OverlayManager::getSingletonPtr();
   Ogre::Overlay *_overlay = _overlayMgr->getByName("overlayMenuState");
   _overlay->getChild("containerMenuState")->setMaterialName("ScreenLoading2");  
}
void MenuState::menu3(int n) 
{
//  createScreenLoading();  
  std::this_thread::sleep_for (std::chrono::seconds(6));
  std::cout << "pause of " << 6 << " seconds ended\n";

  
  
}
void MenuState::menu2(int n) 
{
   std::this_thread::sleep_for (std::chrono::seconds(6)); 
   string name = static_cast<std::ostringstream*>(&(std::ostringstream() <<editBoxName->getText()))->str();
   GameManager::getSingletonPtr()->setNamePlayer(name);
   GameManager::getSingletonPtr()->getTrackMenuPtr()->stop();
   changeState(PlayState::getSingletonPtr());
   //pushState(PlayState::getSingletonPtr());
   
 // std::this_thread::sleep_for (std::chrono::seconds(6));
  std::cout << "pause of " << n << " seconds ended\n";
}


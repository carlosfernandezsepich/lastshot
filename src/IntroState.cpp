#include "IntroState.h"
#include "MenuState.h"

using namespace std;
template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void
IntroState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager"); 
  _sceneMgr->addRenderQueueListener(OGRE_NEW Ogre::OverlaySystem());
  _camera = _sceneMgr->createCamera("IntroCamera");
  _camera->setPosition(Ogre::Vector3(5,100,100));
  _camera->lookAt(Ogre::Vector3(0,0,0));
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  //_viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 1.0));
  renderer = &CEGUI::OgreRenderer::bootstrapSystem(); //For CEGUI
  _exitGame = false;
  createWallPaper();
  //GameManager::getSingletonPtr()->_shootEffect->play();
  //  GameManager::getSingletonPtr()->   //Play main music
  


}

void
IntroState::exit()
{
  Ogre::OverlayManager::getSingletonPtr()->destroyAll();  
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();
}

void
IntroState::pause ()
{
}

void
IntroState::resume ()
{
}

bool
IntroState::frameStarted
(const Ogre::FrameEvent& evt) 
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(evt.timeSinceLastFrame);  
  return true;
}

bool
IntroState::frameEnded
(const Ogre::FrameEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(evt.timeSinceLastFrame);  
  if (_exitGame)
    return false;
  
  return true;
}

void
IntroState::keyPressed
(const OIS::KeyEvent &e)
{
  // Transición al siguiente estado.
  // Espacio --> PlayState
  if (e.key == OIS::KC_SPACE) {
      pushState(MenuState::getSingletonPtr());
  }
}

void
IntroState::keyReleased
(const OIS::KeyEvent &e )
{
  if (e.key == OIS::KC_ESCAPE) {
    _exitGame = true;
  }
}

void
IntroState::mouseMoved
(const OIS::MouseEvent &e)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel); 
}

void
IntroState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
 CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void
IntroState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton IntroState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

IntroState*
IntroState::getSingletonPtr ()
{
return msSingleton;
}

IntroState&
IntroState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

void IntroState::createWallPaper(){
  
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    Ogre::OverlayManager *_overlayMgr = Ogre::OverlayManager::getSingletonPtr();
    Ogre::Overlay *_overlay = _overlayMgr->create("overlayIntroState");
    Ogre::OverlayContainer* _panel = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "containerIntroState"));
    _panel->setMetricsMode(Ogre::GMM_PIXELS);
    _panel->setPosition(0, 0);
    _panel->setDimensions(width, height);
    _panel->setMaterialName("WallPaperIntroState"); // Optional background material
 
    _overlay->add2D(_panel);
 
    _overlay->show();
 
    // GameManager::getSingletonPtr()->getTrackMenuPtr()->play();     //Play main music
  
}


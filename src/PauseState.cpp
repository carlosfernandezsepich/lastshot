#include "PauseState.h"
#include "MenuState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void
PauseState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _viewport = _root->getAutoCreatedWindow()->getViewport(0);
  // Nuevo background colour.
  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 1.0, 0.0));
  createMenu();
  _exitGame = false;
}

void
PauseState::exit ()
{
    deleteMenu();
}

void
PauseState::pause ()
{
    _sheet->setVisible(false);
    _sheet->setEnabled(false);
}

void
PauseState::resume ()
{
    _sheet->setVisible(true);
    _sheet->setEnabled(true);
}

bool
PauseState::frameStarted
(const Ogre::FrameEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(evt.timeSinceLastFrame);   
  return true;
}

bool
PauseState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
PauseState::keyPressed
(const OIS::KeyEvent &e) {
  // Tecla p --> Estado anterior.
    if (e.key == OIS::KC_P) {
     popState();
  }
    if (e.key == OIS::KC_ESCAPE) {
     popState();
    //_exitGame = true;
  }
}

void
PauseState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
PauseState::mouseMoved
(const OIS::MouseEvent &e)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel); 
}

void
PauseState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id)); 
}

void
PauseState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

PauseState*
PauseState::getSingletonPtr ()
{
return msSingleton;
}

PauseState&
PauseState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

CEGUI::MouseButton PauseState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}


void PauseState::createMenu(){

     CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

  CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
  CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("AlfiskoSkin/MouseArrow");

  // Let's make the OS and the CEGUI cursor be in the same place
  CEGUI::Vector2f mousePos = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();  
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(-mousePos.d_x,-mousePos.d_y);
 
  //Sheet
  _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","MenuPauseSheet");
  
  //New Game button
  CEGUI::Window* returnGameButton = CEGUI::WindowManager::getSingleton().createWindow("AlfiskoSkin/Button","MenuPause/ReturnGameButton");
  returnGameButton->setText("Regresar");
  returnGameButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  returnGameButton->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.83,0),CEGUI::UDim(0.3,0)));
  returnGameButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&PauseState::returnPlayState, this));
    //Quit button
  CEGUI::Window* quitButton = CEGUI::WindowManager::getSingleton().createWindow("AlfiskoSkin/Button","MenuPause/QuitButton");
  quitButton->setText("Salir");
  quitButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  quitButton->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.83,0),CEGUI::UDim(0.4,0)));
  quitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&PauseState::quit, this));
  //Attaching buttons
  _sheet->addChild(returnGameButton);
  _sheet->addChild(quitButton);
  CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_sheet);
}

void PauseState::deleteMenu()
{

    CEGUI::WindowManager::getSingleton().destroyWindow(_sheet); 
    _sheet->destroy();
}

bool PauseState::returnPlayState(const CEGUI::EventArgs& e){
  popState();
  return true;
}
bool PauseState::quit(const CEGUI::EventArgs& e){
  popState();
 // exit();
  changeState(MenuState::getSingletonPtr());
  return true;
}
#include <Ogre.h>

#include "GameManager.h"
#include "GameState.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

template<> GameManager* Ogre::Singleton<GameManager>::msSingleton = 0;

GameManager::GameManager ()
{
  _root = 0;
  _initSDL();
  _namePlayer = "Unknown";
}

GameManager::~GameManager ()
{
  while (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }
  
  if (_root)
    delete _root;
}

void
GameManager::start
(GameState* state)
{
  // Creación del objeto Ogre::Root.
  _root = new Ogre::Root();
  
  loadResources();

  if (!configure())
    return;    
  	
  _inputMgr = new InputManager;
  _inputMgr->initialise(_renderWindow);

  // Registro como key y mouse listener...
  _inputMgr->addKeyListener(this, "GameManager");
  _inputMgr->addMouseListener(this, "GameManager");

  // El GameManager es un FrameListener.
  _root->addFrameListener(this);

  // Transición al estado inicial.
  changeState(state);
  loadSounds();

  // Bucle de rendering.
  _root->startRendering();
}

void
GameManager::changeState
(GameState* state)
{
  // Limpieza del estado actual.
  if (!_states.empty()) {
    // exit() sobre el último estado.
    _states.top()->exit();
    // Elimina el último estado.
    _states.pop();
  }

  // Transición al nuevo estado.
  _states.push(state);
  // enter() sobre el nuevo estado.
  _states.top()->enter();
}

void
GameManager::pushState
(GameState* state)
{
  // Pausa del estado actual.
  if (!_states.empty())
    _states.top()->pause();
  
  // Transición al nuevo estado.
  _states.push(state);
  // enter() sobre el nuevo estado.
  _states.top()->enter();
}

void
GameManager::popState ()
{
  // Limpieza del estado actual.
  if (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }
  
  // Vuelta al estado anterior.
  if (!_states.empty())
    _states.top()->resume();
}

void
GameManager::loadResources ()
{
  Ogre::ConfigFile cf;
  cf.load("resources.cfg");
  
  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
  Ogre::String sectionstr, typestr, datastr;
  while (sI.hasMoreElements()) {
    sectionstr = sI.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typestr = i->first;    datastr = i->second;
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation
            (datastr, typestr, sectionstr);	
    }
  }
}

bool
GameManager::configure ()
{
  if (!_root->restoreConfig()) {
    if (!_root->showConfigDialog()) {
      return false;
    }
  }
  
  _renderWindow = _root->initialise(true, "Last Shoot");
  
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
  
  return true;
}

GameManager*
GameManager::getSingletonPtr ()
{
  return msSingleton;
}

GameManager&
GameManager::getSingleton ()
{  
  assert(msSingleton);
  return *msSingleton;
}

// Las siguientes funciones miembro delegan
// el evento en el estado actual.
bool
GameManager::frameStarted
(const Ogre::FrameEvent& evt)
{
  _inputMgr->capture();
  return _states.top()->frameStarted(evt);
}

bool
GameManager::frameEnded
(const Ogre::FrameEvent& evt)
{
  return _states.top()->frameEnded(evt);
}

bool
GameManager::keyPressed 
(const OIS::KeyEvent &e)
{
  _states.top()->keyPressed(e);
  return true;
}

bool
GameManager::keyReleased
(const OIS::KeyEvent &e)
{
  _states.top()->keyReleased(e);
  return true;
}

bool
GameManager::mouseMoved 
(const OIS::MouseEvent &e)
{
  _states.top()->mouseMoved(e);
  return true;
}

bool
GameManager::mousePressed 
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  _states.top()->mousePressed(e, id);
  return true;
}

bool
GameManager::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  _states.top()->mouseReleased(e, id);
  return true;
}

bool GameManager::_initSDL () {
  if (SDL_Init(SDL_INIT_AUDIO) < 0) {
    return false;
  }
  // Llamar a  SDL_Quit al terminar.
  atexit(SDL_Quit);
 
  // Inicializando SDL mixer...
  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,MIX_DEFAULT_CHANNELS, 4096) < 0) {
    return false;
  }
 
  // Llamar a Mix_CloseAudio al terminar.
  atexit(Mix_CloseAudio);
 
  return true;    
}

void GameManager::loadSounds(){

  _pTrackManager = OGRE_NEW TrackManager;
  _pSoundFXManager = OGRE_NEW SoundFXManager;
  _gameWin = _pTrackManager->load("Sound/music/HeroicDemise.mp3");  
  _mainTrackMenu = _pTrackManager->load("Sound/music/IntenseSuspense.mp3");
  _mainTrack = _pTrackManager->load("Sound/music/TurkeyTreeCall.mp3");
  _heridoFX = _pTrackManager->load("audio/others/pr.mp3");
  _menuStateTrack = _pTrackManager->load("audio/music/HeroicDemise.mp3");
  _akShootFX = _pSoundFXManager->load("audio/weapon/ak47-1.wav");
  _akReloadFX = _pSoundFXManager->load("audio/weapon/ak47_boltpull.wav");
  _akEmptyFX = _pSoundFXManager->load("audio/weapon/clip_empty.wav");
  _m9ShootFX = _pSoundFXManager->load("audio/weapon/clip_empty.wav"); //Reemplazar
  _m9ReloadFX = _pSoundFXManager->load("audio/weapon/clip_empty.wav"); //Reemplazar
  _m9EmptyFX = _pSoundFXManager->load("audio/weapon/clip_empty.wav");
  _m4ShootFX = _pSoundFXManager->load("audio/weapon/m4a1_unsil-1.wav");
  _m4ReloadFX = _pSoundFXManager->load("audio/weapon/m4a1_boltpull.wav");
  _m4EmptyFX = _pSoundFXManager->load("audio/weapon/clip_empty.wav");
  _voiceEnemyDownFX = _pSoundFXManager->load("audio/voice/enemydown.wav");
  _voiceLetsgoFX = _pSoundFXManager->load("audio/voice/letsgo.wav");
  _voiceMissionFailedFX = _pSoundFXManager->load("audio/voice/MissionFailed.wav"); 
  _voiceMissionCompletedFX = _pSoundFXManager->load("audio/voice/MissionCompleted.wav");   
  _terroristAttack = _pSoundFXManager->load("audio/voice/terrorist_attack.wav"); 
  _shootEffect = _pSoundFXManager->load("Sound/effects/shoot-weapon.wav");
  _rechargeEffect = _pSoundFXManager->load("Sound/effects/rechange-weapon.wav");
  _emptyEffect = _pSoundFXManager->load("Sound/effects/empty-weapon.wav");  
  
  //_introStateEffect = _pSoundFXManager->load("Sound/effects/impact-cinematic.wav");
  
}
